#!/bin/bash
set -eu
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $SCRIPT_DIR/testlib.sh

seed=$RANDOM
trials=1000
logformat="%s/%s-%d.%s"
prefix=.
multi=

while getopts 's:t:p:m' opt; do
  case $opt in
    s) seed="$OPTARG" ;;
    t) trials="$OPTARG" ;;
    p) prefix="$OPTARG" ;;
    m) multi=1 ;;
  esac
done
shift $((OPTIND-1))

mkdir -p $prefix

for s in diagbroyden broyden1 excitingmixing lm krylov anderson hybr linearmixing; do
  log=`printf $logformat $prefix $s $seed log`
  errlog=`printf $logformat $prefix $s $seed err`
  if [ "$multi" ]; then
    perftest \
      --solver "$s" \
      --seed "$seed" \
      "$trials" "$@" >> "$log" 2>> "$errlog" &
  else
    perftest \
      --solver "$s" \
      --seed "$seed" \
      "$trials" "$@" >> "$log" 2>> "$errlog"
  fi
done
wait

from __future__ import division

from twisty.util import attrdict, timeit
from twisty.runner import Runner
import argparse
import sys
import warnings

DEFAULT_FORMAT = """seed: {seed}
seed curvature: {seed_curvature}
seed torsion: {seed_torsion}
seed step: {seed_step}
seed segments: {seed_segments}

solver: {solver}
solver_tolerance: {solver_tolerance}

total time: {total_time}
total attempts: {total_attempts}

root finding time: {root_finding_time}
root finding time fraction: {root_finding_time_fraction}
function evaluations: {nfev}

failure time: {failure_time}
failure time fraction: {failure_time_fraction}

success time: {success_time}
success time fraction: {success_time_fraction}

failure count: {failure_count}
failure count fraction: {failure_count_fraction}

success count: {success_count}
success count fraction: {success_count_fraction}

curves per core-hour: {curves_per_core_hour}

extrapolation (million): {extrapolation_million_hours}
extrapolation (billion): {extrapolation_billion_hours}
extrapolation (trillion): {extrapolation_trillion_hours}

extrapolation (million) days: {extrapolation_million_days}
extrapolation (billion) days: {extrapolation_billion_days}
extrapolation (trillion) days: {extrapolation_trillion_days}
"""

RAW_FIELDS = (
    "seed",
    "seed_curvature",
    "seed_torsion",
    "seed_segments",
    "seed_step",
    "perturb_range_lower",
    "perturb_range_upper",
    "solver",
    "total_attempts",
    "total_time",
    "nfev",
    "success_count",
    "success_time",
    "failure_time",
    "failure_count",
    "curves_per_core_hour",
)

RAW_HEADER = ",".join(RAW_FIELDS)
RAW_FORMAT = ",".join("{%s}" % f for f in RAW_FIELDS)


def dispatch(args, runner):
    result = attrdict(successes=0, failures=0,
                      seed=runner.seed, solver=runner.solver,
                      scipy=[], nfev=0, success_time=0, failure_time=0)

    # Callback to flush TTYs.
    def callback(**kw):
        if sys.stderr.isatty():
            sys.stderr.write(".")
            sys.stderr.flush()

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        kw = {
            'goal': args.attempts,
            'max_iterations': args.attempts,
            'callback': callback,
        }
        dt, r = timeit(runner.perturb, **kw)

    # Flush at end of trial.
    if sys.stderr.isatty():
        sys.stderr.write("\n")

    results = r
    result.scipy.extend(results)

    for r in results:
        if r.accepted:
            result.successes += 1
            result.success_time += r.time
        else:
            result.failures += 1
            result.failure_time += r.time
        if 'nfev' in r:
            result.nfev += r.nfev

    return result


def main():
    # parse cmd line
    parser = argparse.ArgumentParser()
    parser.add_argument('config', help='Config file to use')
    parser.add_argument('attempts', type=int, help='Number of trials')
    parser.add_argument('--pretty', default=None, help='change output format')
    parser.add_argument('--raw', action='store_true', default=False,
                        help='output raw results as a line of csv')
    parser.add_argument('--headers', action='store_true', default=False,
                        help='output CSV headers and exit')
    args = parser.parse_args()
    runner = Runner.from_config(args.config)

    # set formatting
    fmt = None
    raw = False
    if args.pretty:
        fmt = args.pretty
    elif args.raw:
        fmt = RAW_FORMAT
        raw = True
    fmt = fmt or DEFAULT_FORMAT

    # csv headers special case
    if args.headers:
        print ','.join(RAW_FIELDS)
        sys.exit(0)

    # dispatch
    dt, r = timeit(dispatch, args, runner)
    rt = r.failure_time + r.success_time

    # analysis
    cph = r.successes / (rt / (60 * 60))
    dayf = 1 / (24 * cph) if cph > 0 else float("inf")
    hourf = 1 / cph if cph > 0 else float("inf")

    # output
    d = dict(
        curves_per_core_hour=cph,
        extrapolation_billion_days=1e9 * dayf,
        extrapolation_billion_hours=1e9 * hourf,
        extrapolation_million_days=1e6 * dayf,
        extrapolation_million_hours=1e6 * hourf,
        extrapolation_trillion_days=1e12 * dayf,
        extrapolation_trillion_hours=1e12 * hourf,
        failure_count=r.failures,
        failure_count_fraction=r.failures / args.attempts,
        failure_time=r.failure_time,
        failure_time_fraction=r.failure_time / rt,
        nfev=r.nfev,
        perturb_range_lower=runner.bounds[0] if runner.bounds else None,
        perturb_range_upper=runner.bounds[1] if runner.bounds else None,
        root_finding_time=rt,
        root_finding_time_fraction=rt / dt,
        seed=r.seed,
        seed_curvature=runner.curve.segments[0].curvature,
        seed_segments=len(runner.curve.segments),
        seed_step=runner.curve.step,
        seed_torsion=runner.curve.segments[0].torsion,
        solver=runner.solver,
        solver_tolerance=runner.tol,
        success_count=r.successes,
        success_count_fraction=r.successes / args.attempts,
        success_time=r.success_time,
        success_time_fraction=r.success_time / rt,
        total_attempts=args.attempts,
        total_time=dt,
    )
    print fmt.format(**d)

if __name__ == "__main__":
    main()

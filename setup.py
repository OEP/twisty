from setuptools import setup
import os

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

# Pulls in __version__
exec(open('twisty/__init__.py').read())

setup(
  name='twisty',
  version=__version__,  # noqa
  packages=['twisty'],
  description='Reference implementation for radiative transfer using path '
              'integrals',
  url='https://bitbucket.org/OEP/twisty',
  author='Paul Kilgo',
  author_email='pkilgo@clemson.edu',
  install_requires=[
    'numpy',
    'scipy',
    'PyYAML',
  ],
  # include_package_data=True,
  # license='BSD',
  # long_description=README,
  # zip_safe=False,
  # classifiers=[],
)

#!/bin/bash
#
# Simple analysis of a DAG log.
#
set -eu
if [ $# -eq 0 ]; then
    echo >&2 "Usage: `basename $0` log1 [log2...]"
    exit 1
fi

do_analyze() {
    log=$1
    total=`egrep -o 'Dag contains ([0-9]+) total jobs' $log | head -1 | sed -e 's/[^0-9]//g'`
    finished=`grep 'job complete' $log | wc -l`
    last=`grep 'job complete' $log | tail -5`
    remain=$(( $total - $finished ))
    cat <<EOF
===============================================================================
Analyzing $log
===============================================================================
Complete:    $finished
Remaining:   $remain
Total nodes: $total
Last 5:
$last
EOF
}

for file in "$@"; do
    do_analyze "${file}"
done

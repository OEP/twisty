#!/bin/bash

gzcat() {
    if [[ "$1" == *.csv.gz ]]; then
        gunzip -c "$1"
    else
        cat "$1"
    fi
}

first="$1"
shift
gzcat "$first"

for path in "$@"; do
    gzcat "${path}" | tail -n +2
done

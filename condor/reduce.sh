#!/bin/bash
set -eu
set -o pipefail

if [ ${RETURN-0} -ne 0 ]; then
    exit ${RETURN}
fi

if [ $# -lt 1 ]; then
    echo "Usage: `basename $0` name [name...]"
    exit 1
fi

getfirst() {
    thedir=$1; shift
    (find $thedir -type f "$@" | head -1) || true
}

concat_csv() {
    first=`getfirst $1/csv -name '*.csv'`
    head -1 $first | sed -e 's/^# //'
    find $1/csv -type f -name '*.csv' | xargs -n 40 cat | grep -v ^#
}

xpv() {
    if type pv >/dev/null 2>&1; then
        pv
    else
        cat
    fi
}

do_reduce() {
    test -d "$1" || ( echo "$1: not a directory" >&2; exit 1 )

    if [ -e $1.csv.gz ]; then
        echo "Refusing to overwrite $1.csv.gz. Rename or delete file." >&2
        exit 1
    fi

    echo "Creating $1.csv.gz"
    concat_csv $1 | xpv | gzip > $1.csv.gz

    echo "Cleaning up $1"
    find "$1" -type f -delete
    find "$1/csv" -mindepth 1 -type d -empty -delete
}

for name in "$@"; do
    do_reduce "${name}"
done

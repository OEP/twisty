#!/bin/sh
if [ $# -ne 5 ]; then
  echo "Usage: $0 config runs skip count result" >&2
  exit 1
fi

# Do this ASAP so an output file is available immediately.
set -eu
config=$1
runs=$2
skip=$3
count=$4
result=$5
mkdir -p `dirname "${result}"`
touch "${result}"
set +eu

export http_proxy="${OSG_SQUID_LOCATION}"
if [ -f /etc/redhat-release ]; then
    cat /etc/redhat-release
fi
uname -a
lsb_release -a

use_bundled_python() {
    # Download and extract custom Python if needed.
    if [ ! -e python2.7 ]; then
        url='http://stash.osgconnect.net/+pkilgo/python2.7.sl6.tar.gz'
        tarball='python2.7.sl6.tar.gz'
        if ! wget -nv "${url}" && test ! -e $tarball; then
            echo "Broken proxy?: ${http_proxy}"
            unset http_proxy
            wget -nv "${url}"
        fi
        tar xf ${tarball}
    fi

    # Python environment setup. $HOME is apparently required due to
    # https://bugs.python.org/issue10496.
    PATH=`pwd`/python2.7/bin:${PATH}
    PYTHONHOME=`pwd`/python2.7
    HOME=`pwd`
    LD_LIBRARY_PATH="${PYTHONHOME}/lib"
    export PYTHONHOME HOME LD_LIBRARY_PATH
}

use_oasis_python() {
    module load python/2.7
    module load all-pkgs
}

set -eu
#use_oasis_python
use_bundled_python
type python

# Extract Python libraries and configuration files.
tar xf environment.tar.gz

set +e
for i in `seq ${runs}`; do
  python twisty-sample.py -m 3 $config $skip $count >> $result
  rc=$?
  if [ $rc -ne 0 ]; then
    exit $rc
  fi
done

Radiative transfer using path integrals
=======================================

A reference implementation of using path integrals for radiative transfer. See
[KILGO2016]_ for a formal description.

Requirements
------------

Twisty has been developed for Python 2.7.

Twisty requires the following libraries:

* SciPy_ for most of the numerical work.
* NumPy_ for matrix algebra.
* PyYAML_ for parsing YAML configuration files.

You can usually get these from PyPI using pip:

::

    pip install numpy scipy pyyaml

On Debian-like systems you can install from Apt:

::

    sudo apt-get install python-scipy python-numpy python-yaml

This implementation was meant to be functional on a wide net of these library
versions, but much of the original production data was generated using SciPy
0.16.0 and NumPy 1.9.2.

.. _SciPy: https://www.scipy.org/
.. _NumPy: https://www.numpy.org/
.. _PyYAML: https://pypi.python.org/pypi/PyYAML

Open Science Grid
-----------------

Much of this documentation explains how to use this software on the Open
Science Grid (OSG) Connect login node. If you plan on conducting some
production experiments on OSG, you should get an account there. Sign up on `OSG
Connect`_ and have an existing project member add you to the ``numfpi`` project
on there. Project membership is not required to run experiments, but they grant
group storage space at ``/stash/project/@numfpi`` and perform metrics based on
project usage.

You may see some suggestion to use the `OSG Connect Client`_ to submit your
jobs, but unfortunately the current workflow produces many thousand files and
the Connect Client cannot sync these very efficiently yet. For that, it is
recommended to launch and retrieve the results directly from the login node.

.. _`OSG Connect`: http://osgconnect.net/signup
.. _`OSG Connect Client`: https://support.opensciencegrid.org/support/solutions/articles/5000640327-what-is-the-connect-client-

Experiment workflow
-------------------

This section explains how to collect data primarily using a workflow based in
Condor and the Open Science Grid (OSG). First create two directories at the
root of this Git repository:

::

    mkdir config trials

In ``config`` you will place configuration files for your experiments. You
might consider source-controlling your configuration files and checking out the
repository at this path. The ``trials`` directory contain isolated directories
for the code, configuration, and data of your running jobs in the OSG. This way
if you modify the code while jobs are running, the running jobs are not
affected.

Write the configuration file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first step is to write the configuration file for your experiment. You can
start with the included template file:

::

    cp config.example.yaml config/my-experiment.yaml

The best reference for the available parameters is this template file, and the
Python code itself. Many of the configuration file properties correspond
directly to class constructor parameter names, particularly, anything that
inherits from ``twisty.config.Element``. This is so that you can also define
your own classes and use the same configuration system with just a little
effort.

Often you'll define experiments which use a lot of the same parameters, but
vary one or two at a time. A good strategy is to create a "base" configuration
file called ``base.yaml`` and several other "condition" configuration files
which inherit from this one. For instance:

::

    mkdir config/myexperiment
    cp config.example.yaml config/myexperiment/base.yaml
    vim config/myexperiment/condition1.yaml

And have the condition file inherit from base file:

::

    inherits: 'base.yaml'

    param1: foo
    param2: bar
    ...

Testing configuration
~~~~~~~~~~~~~~~~~~~~~

You can check syntax of configuration files using ``twisty-parse.py``:

::

    python twisty-parse.py config/my-experiment.yaml

This prints its interpretation of the YAML document you provide.

Once you have your configuration file written, you'll want to test it before
running a full scale experiment. You might do this by running the
``twisty-sample.py`` script itself:

::

    python twisty-sample.py config/my-experiment.yaml 1 1

This just outputs a single row of CSV data, which is difficult to interpret. A
simple OpenGL program is available for visualizing how your experiment might
run.

::

    python twisty-show.py config/my-experiment.yaml

There are several commands to modify how this tool works. Press ``?`` while it
is running to print out instructions at any time.

Condor workflow
~~~~~~~~~~~~~~~

Once you are satisfied with the configuration file, you can begin working with
Condor on the Open Science Grid. Log in to the access node, and use a script to
generate submit files needed for Condor. See ``condor/submit.sh -h`` for its
full usage.

::

    condor/submit.sh [-t TOTAL_SAMPLES] config/my-experiment.yaml

This will create a directory called ``trials/my-experiment`` where your job
data goes. By default, a given trial produces (roughly) 400,000 samples. How
many you need varies per experiment, but the beam spread function experiments
routinely needed 10,000,000 to 15,000,000 samples.

.. note::

    At least for OSG Connect, you should avoid having jobs write to the
    ``/home`` mount. This is usually true for a lot of other sites as well. For
    OSG Connect, they prefer that you use either ``/stash/user/$USER``. You can
    either check the entire Git repository here, or symlink ``trials`` so that
    it points to a location in ``/stash``.

Generally you only need to tweak the ``-t`` flag to get your sample count
right. You can change a lot of other things about how the experiment is set up,
for instance, perturbation count, Condor cluster sizes, samples taken per
Condor process/worker.

When you are ready, submit the ``main.dag`` file. Use the ``-maxidle`` to
throttle how many jobs can run on Condor. For example:

::

    cd trials/my-experiment
    condor_submit_dag -maxidle 20 main.dag

.. note::

    The ``condor/submit.sh`` script has an ``-S`` flag which will also submit
    the job as shown above.

The output of the trial is thousands of CSV files. Each row of data corresponds
to a single sample, or an invocation of the ``twisty-sample.py`` script. Each
file contains the following information in its columns:

``time``
    The wall time spent calculating a sample.
``run_id``
    The ``id`` field of the YAML configuration file.
``seed``
    The seed used for the sample.
``step``
    The step size used for all generated paths.
``x1_{x,y,z}``
    The chosen end point constraint.
``n1_{x,y,z}``
    The chosen end tangent constraint.
``segments``
    Path segment count.
``sum``
    Sum of weights of all generated paths.
``count``
    Number of path perturbations completed.

The thousands of CSV files are not very manageable to work with, so there are
scripts to join them described next.

Post-processing
~~~~~~~~~~~~~~~

Each directory in ``trials`` has a ``Makefile`` you can use to collect the
results of all jobs into a gzipped CSV document.

::

    make -C trials/my-experiment

This produces a file, ``trials/my-experiment/my-experiment.csv.gz``, which you
can use for analysis. Depending on the number of files and the speed of the
file system, it can take an hour or two.

You can use ``make clean`` to delete  gzipped CSV file and intermediate files.
To delete everything, use ``make fullclean``. **This will delete job data which
you will have to recompute!**

Analysis
~~~~~~~~

A few useful analysis scripts are provided, which are mainly useful for
analyzing beam spread function calculations. To use them you'll need a few
extra dependencies:

* matplotlib_ for plotting
* pandas_ for data wrangling

As before, install with pip or Apt:

::

    pip install matplotlib pandas
    sudo apt-get install python-matplotlib python-pandas

The gzipped CSV documents are usually quite large since they contain a line of
data for every sample produced. You'll want to reduce this data somehow, and
how you do this is going to vary with the experiment. Provided here are a
couple scripts to work with beam spread functions (BSFs) in a 1D- or 2D-manner,
that is, by angle or xy-coordinate.

Analysing BSFs by angle
~~~~~~~~~~~~~~~~~~~~~~~

Probably the most robust way of looking at the data is by angle. You use the
``bin-angle.py`` script for this:

::

    analysis/bin-angle.py trials/my-experiment/my-experiment.csv.gz \
        >my-bsf.csv

See ``analysis/bin-angle.py --help`` for the full usage. The output of this
script is a CSV file representing a calculated beam spread function with
respect to angle. Rows correspond to a single "bin" mapped to an angle value.
The columns are:

``angle``
    The angle corresponding to this bin, in radians.
``average_weight``
    The average path weight.
``total_weight``
    The sum of all weights.
``weight_{stdev,variance,sem}``
    The weight standard deviation, variance, and standard error of the mean
    (SEM).
``time_{total,mean,sem,stdev,variance}``
    The wall time total, mean, SEM, standard deviation, and variance spent.
``step_{total,mean,sem,stdev,variance}``
    The path step size total, mean, SEM, standard deviation, and variance.
``ok_{total,mean}``
    The total number and mean of successful samples.
``aborted_{total,mean}``
    The total number and mean of aborted samples.
``ok_time_{total,mean,stdev,sem}``
    The total, mean, standard deviation, and SEM of the wall clock time spent
    calculating successful samples.
``aborted_time_{total,mean,stdev,sem}``
    The total, mean, standard deviation, and SEM of the wall clock time spent
    calculating aborted samples.
``ok_step_{total,mean,stdev,sem}``
    The total, mean, standard deviation, SEM of the step sizes of successful
    samples.
``aborted_step_{total,mean,stdev,sem}``
    The total, mean, standard deviation, SEM of the step sizes of aborted
    samples.

You can use ``analysis/plot-angle.py`` to plot this data. See
``analysis/plot-angle.py --help`` for the full usage. The most basic usage
looks like:

::

    # Display the plot in a window
    analysis/plot-angle.py my-bsf.csv

    # Save the plot as a pdf
    analysis/plot-angle.py my-bsf.csv >my-bsf.pdf

Many of the flags are self-explanatory but some of the more important ones are
summarized here:

``--maffione``
    This activates normalization and normalizes to match Figure 5 of
    [MAFFIONE1998]_.
``--normalize``
    Activate normalization. Without this flag, data will not be normalized.
``--normalize-mode sum|peak``
    Either normalize the sum of the values or the peak value of the BSF. It is
    more common to see the peak value normalized.
``--normalize-individually``
    By default, a normalization constant is calculated on the first data set
    and reused for all subsequent ones. This is useful if you would like to
    make relative comparisons between data sets regarding their peaks. If you
    are only interested in the width, use this flag.
``--normalize-to VALUE``
    This changes what value is used for normalization. If you use ``peak``
    normalization, the peak of each data set is set to this value. If you use
    ``sum`` normalization, the sum is normalized to this value. By default, the
    value is 1.
``--lorentzian-fit``
    Fit a Lorentzian function by least-squares. The function used is the
    proposed one in [MAFFIONE1998]_.
``--gaussian-fit``
    Fit a Gaussian function by least-squares. The function used is the proposed
    one in [MAFFIONE1998]_.
``--constrained-gaussian-fit``
    Same as ``--gaussian-fit``, but constrain the fit such that the peak value
    of the Gaussian is the peak value of the data set.
``--add-gaussian WIDTH PEAK``
    Add arbitrary Gaussian fits to the plot. This can be useful to see how
    closely the calculated data follow a model not predicted by least-squares.

You can also produce residuals plots for least-squares Gaussian fits. The
script to do this is limited in function; the common usage looks like:

::

    # Show plot in a window
    analysis/plot-residuals.py [--normalize-to VALUE] my-bsf.csv

    # Save plot to pdf
    analysis/plot-residuals.py [--normalize-to VALUE] my-bsf.csv >residuals.pdf

Analysing BSFs by xy-coordinate
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Similarly there is a ``analysis/bin-xy.py`` script for creating a 2D projection
of BSF data. This projection is mainly useful for checking for spherical
symmetry. See ``analysis/bin-xy.py --help`` for the full usage. For example:

::

    analysis/bin-xy.py trials/my-experiment/my-experiment.csv.gz \
        >my-bsf-2d.csv

This produces a CSV file with the following columns:

``x``
    The x-coordinate (``dot(n1, camera.right)``) for this bin
``y``
    The y-coordinate (``dot(n1, camera.up)``) for this bin
``weight_{total,mean}``
    Total and mean of path weights.
``time_{total,mean}``
    Total and mean of wall time spent.
``step_{total,mean}``
    Total and mean of path step size.
``ok_{total,mean}``
    Total and mean of successful samples.
``aborted_{total,mean}``
    Total and mean of aborted samples.
``ok_time_{total,mean}``
    Total and mean of wall time spent for successful samples.
``aborted_time_{total,mean}``
    Total and mean of wall time spent for aborted samples.
``ok_step_{total,mean}``
    Total and mean of path step size for successful samples.
``aborted_step_{total,mean}``
    Total and mean of path step size for aborted samples.

The script ``analysis/plot-xy.py`` can plot this data. See
``analysis/plot-xy.py --help`` for the full usage. Some examples follow:

::

    # Show plot in a window
    analysis/plot-xy.py [--normed] my-bsf-2d.csv

    # Save plot to pdf
    analysis/plot-xy.py [--normed] my-bsf-2d.csv >bsf-2d.pdf

    # Use the cubehelix color map
    analysis/plot-xy.py --cmap cubehelix my-bsf-2d.csv


Using R
~~~~~~~

RStudio_ is also very nice for analysis. You may try experimenting with it to
do something not supported with the scripts here. R tends to get bogged down
with very large data sets, so beware, you may want to reduce the data somehow
first by using ``analysis/bin-angle.py`` or something similar.

.. _pandas: http://pandas.pydata.org/
.. _matplotlib: http://matplotlib.org/
.. _RStudio: https://www.rstudio.com/

Running unit tests
------------------

If you'd like to keep it simple, a traditional script is supplied.

::

    ./runtests.sh [--failfast] [--verbose]

A ``tox.ini`` file is provided for running unit tests in isolated environments.
To install tox

::

    pip install tox
    # or...
    sudo apt-get install python-tox

Then you can run the unit tests using tox:

::

    # Just run them
    tox

    # Run in the 'coverage' environment
    tox -e coverage

    # Pass in arguments to unittest.discover
    tox  -- [--failfast] [--verbose]

References
==========

.. [KILGO2016] Paul Kilgo. "Radiative transfer using path integrals for
    multiple scattering in participating media". PhD thesis. School of
    Computing, Clemson University, Dec. 2016.
.. [MAFFIONE1998] Robert A. Maffione, Jeff M. Voss, and Curtis D. Mobley.
    "Theory and measurements of the complete beam spread function of sea ice."
    Limnology and Oceanography, 43(1):34– 43, 1998.
#!/usr/bin/env python
from __future__ import division, print_function
import sys
from OpenGL.GL import *  # noqa
from OpenGL.GLU import *  # noqa
from OpenGL.GLUT import *  # noqa
from PIL import Image, ImageDraw, ImageFont
import numpy as np

import math
import pprint
from collections import defaultdict
from argparse import ArgumentParser

import twisty.fs
from twisty.gl import draw_curve, draw_histogram, draw_grid
from twisty.stat import Histogram
from twisty.runner import Runner
from twisty.util import attrdict, timeit
from twisty import geometry, bootstrap

# histogram display constants
NONE, WEIGHTS, PARAMETERS, BOTH = HISTOGRAM_MODES = range(4)

# Log level parameters
DEBUG, INFO, WARN, ERROR = 10, 20, 30, 40

# XXX These constants are not standard
WHEEL_UP, WHEEL_DOWN = 3, 4

XYZ_COLORS = [
    (1, 0, 0, 1),
    (1, 1, 0, 1),
    (0, 0, 1, 1),
]

SPHERE_QUADRIC = gluNewQuadric()

USAGE = '''\
Mouse commands:

    Left mouse button: Tumble
    Scroll wheel:      Zoom

Keyboard commands:

    c: Continuously perturb the curve.
    d: Enter debug prompt.
    e: Create a new seed path.
    f: Output frames of the OpenGL buffer.
    g: Toggle grid.
    h: Toggle curve history.
    m: Rotate histogram mode.
    n: Perturb curve one time.
    p: Print metadata.
    r: Toggle rotation minimizing frames.
    s: Toggle geometry display (if available).
    t: Toggle turntable.
    x: Toggle origin display.
    z: Reset the view.
    1: Toggle "space curve" seed path display.
    2: Toggle seed path display.
    ?: Print this message.
'''

runner = None
opt = attrdict()
opt.current_frame = 1
opt.frame_format = "out.%04d.png"
opt.histogram_display = NONE
opt.histograms = defaultdict(Histogram)
opt.histograms['curve_weights'] = Histogram(1e-3)
opt.history = []
opt.history_max = 256
opt.meta = attrdict()
opt.refresh_rate = int(1000 / 30)
opt.rotate_x = 0.0
opt.rotate_y = 0.2
opt.turntable_rate = math.pi / 10
opt.want_continuous = False
opt.want_frames = False
opt.want_geometry = True
opt.want_grid = True
opt.want_xyz = False
opt.want_history = False
opt.want_rmf = False
opt.want_turntable = False
opt.want_template = True
opt.want_original = True
opt.zoom = 1.
opt.log_level = INFO

ui = attrdict()
ui.rotating = False

WARNING_FORMAT = """
{sep}
Something seems to be the matter!
{sep}

The solution {did_or_did_not} converge, yet...

- The absolute difference in the end point is {err1}
- The absolute difference in the end tangent is {err2}

Here is the result from scipy:

{result}
"""


def log(level, *args, **kw):
    if level >= opt.log_level:
        print(*args, **kw)


def debug(*args, **kw):
    log(DEBUG, *args, **kw)


def info(*args, **kw):
    log(INFO, *args, **kw)


def warn(*args, **kw):
    log(WARN, *args, **kw)


def setup(args):
    global runner
    runner = Runner.from_config(args.config)
    opt.meta.update(vars(args))
    opt.meta.T0 = runner.curve.basis[0]
    opt.meta.N0 = runner.curve.basis[1]
    opt.meta.B0 = runner.curve.basis[2]
    opt.meta.seed = runner.seed
    opt.meta.goalt = runner.constraints.n1
    opt.meta.goalp = runner.constraints.x1
    opt.original = tuple(runner.curve.points())
    opt.meta.endt = runner.curve.end_basis()[0]
    opt.meta.endp = runner.curve.end_point()


def _perturb_callback(**kw):
    opt.meta.indices = kw.get('indices')


def dispatch_perturb(iterations):
    debug('Perturbing...')
    dt, result = timeit(
        runner.perturb,
        goal=iterations,
        callback=_perturb_callback
    )
    debug('done.')

    if not result[-1].success:
        debug('Last attempt ended in failure!')
    else:
        opt.meta.total_curves = opt.meta.setdefault('total_curves', 0) + 1
        debug('Total:', opt.meta.total_curves)
        opt.history.append(tuple(runner.curve.points()))
        if len(opt.history) > opt.history_max:
            opt.history[:] = opt.history[len(opt.history) - opt.history_max:]

        # weight calculation
        weights = runner.node_weights
        weight = runner.path_weight

        # dynamically compute the scale from the first weight generated
        if opt.get('path_weight_scale') is None:
            opt.path_weight_scale = weight

        weight_scaled = weight / opt.path_weight_scale
        opt.meta.weight = weight
        opt.meta.weight_scaled = weight_scaled
        opt.meta.weight_sum = weight + opt.meta.setdefault('weight_sum', 0.0)
        opt.meta.acceptance_probability = result[-1].importance

        debug("Acceptance probability:", result[-1].importance)
        debug("Accepted:", result[-1].accepted)
        debug("Weight:", weight)
        debug("Weight scaled:", weight_scaled)
        debug("Weight sum:", opt.meta.weight_sum)
        debug("MinWeight:", min(weights))
        debug("MaxWeight:", max(weights))

        # record new info in histograms
        indices = opt.meta.get('indices', ())
        for i in indices:
            opt.histograms['node_weights'].add(weights[i])
            opt.histograms['curvatures'].add(
                runner.curve.segments[i].curvature)
            opt.histograms['torsions'].add(runner.curve.segments[i].torsion)
        try:
            opt.histograms['curve_weights'].add(weight_scaled)
        except (ValueError, OverflowError):
            debug('Notice: Skipped adding %s to histogram.' % weight)

    # record timing information
    debug("Average time: %.3f" % (dt / iterations))
    opt.meta.t_cvg = dt
    return result


def dispatch_continuous():
    result = dispatch_perturb(1)
    opt.meta.endp = endp = runner.curve.end_point()
    opt.meta.endt = endt = runner.curve.end_basis()[0]
    dx = endp - runner.constraints.x1
    dn = endt - runner.constraints.n1
    tol = 0.1
    err1 = sum(dx ** 2)
    err2 = sum(dn ** 2)
    opt.meta.err1 = err1
    opt.meta.err2 = err2
    if err1 > tol or err2 > tol:
        kw = {'sep': '*' * 79,
              'did_or_did_not': "did" if result[-1].success else "did not",
              'result': result[-1],
              'err1': err1,
              'err2': err2,
              }
        warn(WARNING_FORMAT.format(**kw))
        opt.want_continuous = False


def write_dict(image, d, xy=(0, 0), fill='red', pad=2):
    d = dict((str(k), str(v)) for k, v in d.items())
    xy = list(xy)
    draw = ImageDraw.Draw(image)
    font = ImageFont.load_default()
    w, h = font.font.getsize("")
    for k, v in sorted(d.items()):
        l = "{k}: {v}".format(k=k, v=v)
        draw.text(xy, l, font=font, fill=fill)
        xy[1] += (h + pad)


def dispatch_write_frame():
    glPixelStorei(GL_PACK_ALIGNMENT, 1)
    data = glReadPixels(0, 0, opt.width, opt.height, GL_RGBA, GL_UNSIGNED_BYTE)
    image = Image.fromstring("RGBA", (opt.width, opt.height), data)
    image = image.transpose(Image.FLIP_TOP_BOTTOM)
    write_dict(image, opt.meta)
    image.save(opt.frame_format % opt.current_frame,
               opt.frame_format.rsplit(".", 1)[-1].upper())
    debug("Wrote %s" % (opt.frame_format % opt.current_frame))
    opt.current_frame += 1


def init():
    pass


def reset_projection():
    """ Place camera a clip pane length away, with a tilt of about theta.
    """
    width, height = glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60.0 * opt.zoom, width / height, 0.1, 100.0)
    l = 100.0 / 2
    theta = 30.0 * math.pi / 180.0
    lz, ly = l * math.cos(theta), l * math.sin(theta)
    gluLookAt(0, ly, lz,
              0, 0, 0,
              0, 1, 0)


def keyboard(key, x, y):
    def _toggle(label, state):
        info('%s: %s' % (label, 'ON' if state else 'OFF'))
    if key.lower() == 'c':
        opt.want_continuous = not opt.want_continuous
        _toggle('Continuous', opt.want_continuous)
    if key.lower() == 'd':
        import pdb
        pdb.set_trace()
    if key.lower() == 'e':
        runner.reset()
        glutPostRedisplay()
    if key.lower() == 'f':
        opt.want_frames = not opt.want_frames
        _toggle('Frame output', opt.want_frames)
    if key.lower() == 'g':
        opt.want_grid = not opt.want_grid
        _toggle('Grid', opt.want_grid)
        glutPostRedisplay()
    if key.lower() == 'h':
        opt.want_history = not opt.want_history
        _toggle('History', opt.want_history)
    if key.lower() == 'm':
        opt.histogram_display = (
            opt.histogram_display + 1) % len(HISTOGRAM_MODES)
    if key.lower() == 'n':
        dispatch_perturb(1)
        glutPostRedisplay()
    if key.lower() == 'p':
        pprint.pprint(opt.meta)
    if key.lower() == 'r':
        opt.want_rmf = not opt.want_rmf
        _toggle('Rotation minimizing frame', opt.want_rmf)
    if key.lower() == 's':
        opt.want_geometry = not opt.want_geometry
        _toggle('Geometry', opt.want_geometry)
        glutPostRedisplay()
    if key.lower() == 't':
        opt.want_turntable = not opt.want_turntable
        _toggle('Turntable', opt.want_turntable)
    if key.lower() == 'x':
        opt.want_xyz = not opt.want_xyz
        _toggle('Origin XYZ', opt.want_xyz)
        glutPostRedisplay()
    if key.lower() == 'z':
        info('Resetting view')
        reset_view()
        glutPostRedisplay()
    if key.lower() == '1':
        opt.want_template = not opt.want_template
        _toggle('Template', opt.want_template)
        glutPostRedisplay()
    if key.lower() == '2':
        opt.want_original = not opt.want_original
        _toggle('Original', opt.want_original)
        glutPostRedisplay()
    if ord(key) == 27:
        sys.exit(0)
    if key.lower() == '?':
        print('=' * 80)
        print(USAGE)
        print('=' * 80)


def timer(value):
    if opt.want_turntable:
        opt.rotate_y += opt.turntable_rate
        glutPostRedisplay()
    if opt.want_continuous:
        dispatch_continuous()
        glutPostRedisplay()
    glutTimerFunc(opt.refresh_rate, timer, 0)


def mouse(button, state, x, y):
    if button == GLUT_LEFT_BUTTON:
        ui.rotating = state == GLUT_DOWN
    elif button == WHEEL_UP:
        opt.zoom += 0.01
        glutPostRedisplay()
    elif button == WHEEL_DOWN:
        opt.zoom = max(0.01, opt.zoom - 0.01)
        glutPostRedisplay()
    if ui.rotating:
        ui.original_rotate = opt.rotate_x, opt.rotate_y
        ui.rotate_start = x, y


def motion(x, y):
    if ui.rotating:
        opt.rotate_x = ui.original_rotate[0] + 0.1 * (ui.rotate_start[1] - y)
        opt.rotate_y = ui.original_rotate[1] + 0.1 * (ui.rotate_start[0] - x)
        glutPostRedisplay()


def display():
    reset_projection()
    glMatrixMode(GL_MODELVIEW)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()

    glRotate(opt.rotate_x, 1, 0, 0)
    glRotate(opt.rotate_y, 0, 1, 0)

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
    glColor(1, 1, 1, 1)
    if opt.want_grid:
        draw_grid(10, 10)
    if opt.want_xyz:
        draw_xyz()

    # original curve
    if opt.want_original:
        glColor(0, 1, 1, 1)
        draw_curve(opt.original)

    # the current curve
    glColor(0, 1, 0, 1)
    draw_curve(runner.curve.points())

    # template curve
    if opt.want_template and 'parametric' in runner.constraints.meta:
        p = runner.constraints.meta['parametric']
        template = p.position(np.linspace(0, p.length, 200))
        glColor(1, 0, 0, 1)
        draw_curve(template)

        # constraints
        glBegin(GL_POINTS)
        glColor(1, 0, 0, 1)
        glVertex3f(*runner.constraints.x1)
        glEnd()

    # rmf
    if opt.want_rmf:
        points = list(runner.curve.points())
        basis = np.roll(runner.curve.basis, 2, axis=0)
        bases = twisty.fs.rmf(points, basis)
        for point, basis in zip(points, bases):
            draw_basis(point, basis, 2 * runner.curve.step, XYZ_COLORS)

    if opt.want_history:
        for i, c in enumerate(reversed(opt.history)):
            q = i / float(opt.history_max)
            h = 120.0 * (1 - q) ** 4
            r, g, b = _hsv_to_rgb((h, 1.0, 1.0))
            glColor(r, g, b, 1 - q)
            draw_curve(c)

    if opt.want_geometry:
        draw_scene()

    # dots of perturbation
    glEnable(GL_PROGRAM_POINT_SIZE)
    glPointSize(5.0)
    glBegin(GL_POINTS)
    glColor(1, 1, 1, 1)
    for p in runner.curve.report(opt.meta.get('indices', ())):
        glVertex3f(*p)
    glEnd()

    # histograms
    glPushMatrix()
    glLoadIdentity()
    glScalef(4, 4, 4)
    glTranslatef(1.4, 1, 0)
    if opt.histogram_display in (WEIGHTS, BOTH):
        glPushMatrix()
        draw_histogram(opt.histograms['node_weights'], logscale=10)
        glTranslatef(-1.1, 0, 0)
        draw_histogram(opt.histograms['curve_weights'], logscale=10)
        glPopMatrix()
    if opt.histogram_display in (PARAMETERS, BOTH):
        glPushMatrix()
        glTranslatef(0, -1.1, 0)
        draw_histogram(opt.histograms['torsions'], tickx=10)
        glTranslatef(-1.1, 0, 0)
        draw_histogram(opt.histograms['curvatures'], tickx=10)
        glPopMatrix()
    glPopMatrix()

    # write frames
    if opt.want_frames:
        dispatch_write_frame()

    glutSwapBuffers()


def draw_scene():
    glColor(1, 1, 1, 0.05)
    scene = runner.bootstrapper
    if not isinstance(scene, bootstrap.SimpleGeometry):
        return
    draw_geometry(scene.emitter)
    draw_geometry(scene.sensor)


def draw_geometry(g):
    if isinstance(g, geometry.Sphere):
        draw_sphere(g)
    elif isinstance(g, geometry.Ray):
        draw_ray(g)
    else:
        raise TypeError('Cannot draw object of type %r' % type(g))


def draw_sphere(g):
    gluSphere(SPHERE_QUADRIC, g.radius, 20, 20)


def draw_ray(g):
    pass


def draw_basis(x, basis, scale, colors=None):
    colors = colors or [None] * 4
    glBegin(GL_LINES)
    for c, v in zip(colors, basis):
        if c:
            glColor(*c)
        glVertex3f(*x)
        glVertex3f(*(x + scale * v))
    glEnd()


def draw_xyz(scale=1):
    draw_basis(np.zeros(3), np.eye(3), scale, colors=XYZ_COLORS)


def reset_view():
    opt.rotate_x = 0.0
    opt.rotate_y = 0.2
    opt.zoom = 1.


def _hsv_to_rgb(hsv):
    h, s, v = hsv
    hp = h / 60.0
    c = v * s
    x = c * (1 - abs(hp % 2 - 1))
    if 0 <= hp < 1:
        return c, x, 0
    elif 1 <= hp < 2:
        return x, c, 0
    elif 2 <= hp < 3:
        return 0, c, x
    elif 3 <= hp < 4:
        return 0, x, c
    elif 4 <= hp < 5:
        return x, 0, c
    elif 5 <= hp < 6:
        return c, 0, x
    else:
        return 0, 0, 0


def main():
    glutInit(sys.argv)
    parser = ArgumentParser()
    parser.add_argument('config', help='Config file to use')
    parser.add_argument('--dimensions', nargs=2, type=int, default=(960, 540))
    parser.add_argument('--fullscreen', action='store_true')
    args = parser.parse_args()
    setup(args)
    opt.width, opt.height = args.dimensions

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
    if args.fullscreen:
        glutEnterGameMode()
    else:
        glutInitWindowSize(*args.dimensions)
        glutCreateWindow('Path integral visualizer')

    glClearColor(0., 0., 0., 1)
    glutDisplayFunc(display)
    glutKeyboardFunc(keyboard)
    glutMotionFunc(motion)
    glutMouseFunc(mouse)
    glutTimerFunc(opt.refresh_rate, timer, 0)

    # alpha blending
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    # depth buffer
    glEnable(GL_DEPTH_TEST)
    glDepthMask(GL_TRUE)
    glDepthFunc(GL_LEQUAL)

    # fancy rendering
    glLineWidth(2.0)
    glShadeModel(GL_SMOOTH)

    init()
    print(USAGE)
    glutMainLoop()

if __name__ == "__main__":
    main()

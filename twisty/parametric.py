''' Parametric forms for generating paths.
'''

from __future__ import division

from .curve import ConstrainedCurve

import numpy as np
import scipy
from scipy import misc


def sine_step(s, freq=0):
    gamma = 0.5 * (4 * freq + 1) * scipy.pi
    return scipy.sin(s * gamma)


class Parametric(ConstrainedCurve):
    """
    This is an old attempt generating seed paths, largely replaced with Bezier
    curves.

    Parameters:
      freq: an integer whose absolute value affects the arc length
            (default: 0)
      step: any step function which satisfies
              * f(0) = 0
              * f(1) = 1
              * f'(1) = 0
    """

    def __init__(self, x0, x1, n0, n1, freq=0, step=None, *args, **kw):
        super(Parametric, self).__init__(*args, **kw)
        self.x0 = np.asarray(x0)
        self.x1 = np.asarray(x1)
        self.n0 = np.asarray(n0)
        self.n1 = np.asarray(n1)
        self.freq = freq
        self.step = step or (lambda s: sine_step(s, freq=self.freq))

    def position(self, s):
        """
        Generate a position along the curve.
        """
        s = np.asarray(s)
        if len(s.shape) == 1:
            ss = s.reshape(s.shape[0], 1)
        else:
            ss = s
        alpha = 0.5 * (4 * self.freq + 1) * scipy.pi
        beta = alpha
        sn = ss / self.length

        g0 = self.x1 - self.x0 - (self.length / alpha) * (self.n0 + self.n1)
        gs = g0 * (scipy.sin(sn * beta) ** 2) * self.step(sn)

        a = (self.length * self.n0 / alpha) * scipy.sin(sn * alpha)
        b = (self.length * self.n1 / alpha) * (1 - scipy.cos(sn * alpha))

        return self.x0 + a + b + gs


class Bezier(ConstrainedCurve):
    '''
    Standard n-degree Bezier curve implementation.

    Parameters
    ----------

    control: list
        The list of control points.

    Other parameters are passed to superclass.
    '''

    def __init__(self, control=None, *args, **kw):
        super(Bezier, self).__init__(*args, **kw)
        self.control = list(control) if control is not None else []

    @property
    def control(self):
        return self._control

    @control.setter
    def control(self, value):
        self._control = [np.asarray(v) for v in value]

    def position(self, s):
        s = np.asarray(s)
        if len(s.shape) == 1:
            ss = s.reshape(s.shape[0], 1)
        else:
            ss = s

        t = ss / self.length
        tt = 1. - t
        result = np.zeros((ss.shape[0], 3) if ss.shape else 3)

        n = len(self.control) - 1
        for i, p in enumerate(self.control):
            result += misc.comb(n, i) * (tt ** (n - i)) * (t ** i) * p

        return result

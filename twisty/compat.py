"""
Compatibility hacks.

"""

import scipy.optimize

import warnings

from .util import attrdict


def root(fn, guess, *args, **kw):
    """
    Backwards compatible interface for `scipy.optimize.root`.


    What's now known as scipy.optimize.root is just a big wrapper among all the
    solvers which scipy supports. It's currently best practice to use Powell's
    hybrid method for this application, so we can fall back on
    scipy.optimize.fsolve if it's present.

    """
    if hasattr(scipy.optimize, 'root'):
        return scipy.optimize.root(fn, guess, *args, **kw)
    warnings.warn('Falling back on scipy.optimize.fsolve')
    x = scipy.optimize.fsolve(fn, guess)
    return attrdict(
        x=x,
        fun=fn(x),
        success=True,  # perturb() does not trust this result anyway
    )

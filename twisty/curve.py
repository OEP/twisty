'''
Basic curve implementation.

This implements a traditional continuous curve in the mathematical sense:

  r(t) = <x(t), y(t), z(t)>

'''
from . import fs
from .gfx import unit, normalize

import numpy as np
from scipy import integrate, optimize, misc

from abc import ABCMeta, abstractmethod
import math


class ConvergenceError(Exception):
    pass


class Curve(object):
    '''
    Abstract base class for a general space curve. Objects inheriting from this
    class must define a position functin and gain several other properties of
    space curves which are numerically calculated from the position function.

    Example:

        >>> class Line(Curve):
        ...     def position(self, s):
        ...         return np.array([s, 0, 0], dtype=np.float)
        >>> line = Line()
        >>> line.position(1)
        array([1., 0, 0])
        >>> line.tangent(2)
        array([1., 0, 0])
        >>> line.arclength(2)
        2
    '''
    __metaclass__ = ABCMeta

    @abstractmethod
    def position(self, s):
        """
        Generate a position along the curve.

        """
        raise NotImplementedError

    def tangent(self, s, **kw):
        return normalize(self.grad(s, **kw))

    def grad(self, s, **kw):
        kw.setdefault('dx', 1e-6)
        return misc.derivative(self.position, s, **kw)

    def grad2(self, s, **kw):
        kw.setdefault('dx', 1e-6)
        kw.setdefault('order', 3)
        return misc.derivative(self.position, s, n=2, **kw)

    def grad3(self, s, **kw):
        kw.setdefault('dx', 1e-6)
        kw.setdefault('order', 5)
        return misc.derivative(self.position, s, n=3, **kw)

    def curvature(self, s):
        s = np.asarray(s)

        # Coerce d2x/ds2 and dx/ds into column vectors.
        dx = self.grad(s)
        if len(dx.shape) == 1:
            dx = dx.reshape(1, 3)
        d2x = self.grad2(s)
        if len(d2x.shape) == 1:
            d2x = d2x.reshape(1, 3)

        # Compute the result.
        norm = np.linalg.norm
        top = norm(np.cross(dx, d2x, axis=1), ord=2, axis=1)
        bottom = norm(dx, ord=2, axis=1) ** 3
        result = top / bottom

        # Coerce result into input argument's shape.
        if s.shape:
            return result.reshape(*s.shape)
        else:
            assert result.shape == (1,)
            return np.asarray(result[0])

    def torsion(self, s, force=False, dx=0.1):
        """
        Calculate torsion of parametric curve.

        This method has a known issue where array-like calculations do not
        agree with scalar calculations. The `s' argument is checked to avoid
        unintentional errors in calculation. You can disable the check with
        `force=True'.

        Parameter:
          s: position along curve
          force: set to true to ignore argument checking

        """
        s = np.asarray(s)
        if not force and s.shape != ():
            raise ValueError('array-like arguments not yet supported')

        d1 = self.grad(s, dx=dx)
        d2 = self.grad2(s, dx=dx)
        d3 = self.grad3(s, dx=dx)
        cross = np.cross(d1, d2)

        # Torsion is undefined.
        if np.allclose(0 * cross, cross):
            return 0 * s

        top = cross.dot(d3.T)
        if len(top.shape) == 2:
            top = top.sum(axis=1)

        bottom = cross.dot(cross.T)
        if len(bottom.shape) == 2:
            bottom = bottom.sum(axis=1)

        return np.asarray(top / bottom)

    def speed(self, s):
        """
        Calculate the magnitude of the first derivative.

        Parameters:
          s: the curve position parameter

        """
        s = np.asarray(s)
        return np.linalg.norm(self.grad(s), 2)

    def arclength(self, s):
        """
        Calculate the arclength of the curve

        Parameters:
          s: the curve position parameter

        """
        y, abserr = integrate.quad(self.speed, 0, s)
        return y


class ConstrainedCurve(Curve):

    '''
    Curve defined only on the interval [0, length].

    '''

    def __init__(self, length):
        self.length = length

    def argmap(self, l, a=None, b=None, arclength=None, **kw):
        """
        Given `l', calculate `s' such that arclength(s) == l.

        Parameters:
          l: the desired curve arclength
          eps: tolerance value

        """
        if arclength is None:
            arclength = self.arclength(self.length)
        if a is None:
            a = 0
        if b is None:
            b = self.length
        if not 0 <= l <= arclength:
            raise ValueError('l must be in range [0, arclength(length)]')
        try:
            x = optimize.brentq(lambda s: self.arclength(s) - l, a, b, **kw)
        except RuntimeError as e:
            raise ConvergenceError(e.message)

        return float(x)

    def argsample(self, nb, table=20, table_min=2, **kw):
        """
        Calculate an ndarray in s-space evenly spaced over [0, length]

        Parameters:
          nb: integer number of arclength subintervals
          table: factor by which nb is divided to calculate table size
          table_min: the minimum size of table used for interpolation

        """
        if nb < 2:
            raise ValueError('nb must be greater than or equal to 2')
        try:
            arclength = kw['arclength']
        except KeyError:
            arclength = kw['arclength'] = self.arclength(self.length)

        # Calculate the edges of the approximation table.
        table_size = max(table_min, int(math.ceil(nb / float(table))))
        l_table = np.linspace(0, arclength, table_size)
        dl = arclength / (table_size - 1)

        # Compute the tabled values.
        s_table = []
        for li in l_table:
            si = self.argmap(li, **kw)
            s_table.append(si)

        # Linearly interpolate over the table values to compute.
        s = []
        i_max = len(s_table) - 1
        for li in np.linspace(0, arclength, nb):
            idx = li / dl
            q = idx - math.floor(idx)
            i0 = int(idx)
            i1 = min(i_max, i0 + 1)
            f0 = s_table[i0]
            f1 = s_table[i1]
            si = q * f1 + (1 - q) * f0
            s.append(si)
        return np.asarray(s)

    def to_fs(self, resolution, parameter=False):
        """
        Convert this parametric form into a discrete Frenet-Serret object.

        Parameters:
          resolution: sample count along parmetric form
          parameter: return the FS object and curve parameter as 2-tuple

        """
        # Slow part -- evenly sample the parametric function.
        arg = self.argsample(resolution)

        # Construct an orthonormal basis for the Frenet-Serret curve.
        d1 = self.grad(0)
        d2 = self.grad2(0)
        tangent = unit(d1)
        binormal = unit(np.cross(d1, d2))
        normal = np.cross(binormal, tangent)
        basis = np.array([tangent, normal, binormal])

        # Compute the step size.
        step = self.arclength(self.length) / (resolution + 1)

        # Now actually make the Frenet-Serret curve.
        x0 = self.position(0)
        curve = fs.FS(basis=basis, step=step, origin=x0)
        segments = []
        for s in arg:
            curvature = self.curvature(s)
            torsion = self.torsion(s)
            segment = fs.FS.Segment(curvature, torsion)
            segments.append(segment)
        curve.segments = segments
        if parameter:
            return curve, arg
        else:
            return curve

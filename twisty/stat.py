""" Statistics and analysis routines.
"""

import numpy as np


class Histogram(object):

    def __init__(self, scale=0.01):
        self._scale = scale
        self._bins = {}
        self.llc = np.array([np.inf, 0])
        self.urc = np.array([-np.inf, -np.inf])

    @property
    def scale(self):
        return self._scale

    @property
    def empty(self):
        return not bool(self._bins)

    def add(self, value):
        if value in map(float, ['inf', 'nan', '-inf']):
            raise ValueError("Can't add %s to histogram." % value)
        binid = int(value / self.scale)
        value = binid * self.scale
        self._bins[binid] = 1 + self._bins.setdefault(binid, 0)
        self._update_bounds(value, self._bins[binid])
        return self._bins[binid]

    def walk(self):
        """ Yields (bin_edge, count) on current histogram data.
        """
        keys = sorted(self._bins.keys())
        if not keys:
            return
            yield

        # yield first item
        prev = keys[0]
        yield prev * self.scale, self._bins[prev]

        for k in keys:
            # if we skipped a few, yield the zeroes in between
            if k > prev + 1:
                yield (prev + 1) * self.scale, 0
                if k > prev + 2:
                    yield (k - 1) * self.scale, 0

            # update and loop
            yield k * self.scale, self._bins[k]
            prev = k

    def _update_bounds(self, x, y):
        xy = np.array((x, y))
        self.urc = np.fmax(self.urc, xy)
        self.llc = np.fmin(self.llc, xy)

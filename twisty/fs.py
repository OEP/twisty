"""Discrete Frenet-Serret implementation.
"""
from __future__ import division

from .compat import root

import numpy as np

import math
import random
import time
from functools import reduce


class Segment(object):
    '''
    A Frenet-Serret path segment. This implements some caching of the
    transition matrix so it is not needlessly recomputed.

    '''

    def __init__(self, curvature, torsion, curve=None):
        self.curvature = curvature
        self.torsion = torsion
        self.curve = curve

    @property
    def transition_matrix(self):
        try:
            return self._transition_matrix
        except AttributeError:
            u = FS.get_transition_matrix(self.curvature,
                                         self.torsion,
                                         self.curve.step)
            self._transition_matrix = u
            return self._transition_matrix

    @transition_matrix.deleter
    def transition_matrix(self):
        '''Expire the transition matrix cache'''
        try:
            del self._transition_matrix
        except AttributeError:
            pass

    @property
    def curvature(self):
        return self._curvature

    @curvature.setter
    def curvature(self, value):
        self._curvature = value
        del self.transition_matrix

    @property
    def torsion(self):
        return self._torsion

    @torsion.setter
    def torsion(self, value):
        self._torsion = value
        del self.transition_matrix

    def __iter__(self):
        '''For tuple() compatibility'''
        yield self.curvature
        yield self.torsion

    def __str__(self):
        return "<Segment: curvature=%s, torsion=%s>" % (self.curvature,
                                                        self.torsion)

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return (isinstance(self, type(other)) and
                self.curvature == other.curvature and
                self.torsion == other.torsion)


class FrenetSerret(object):
    '''
    A Frenet-Serret discrete curve.

    '''
    Segment = Segment

    basis = None
    step = None
    segments = ()

    def __init__(self, basis, step, origin=(0, 0, 0), segments=()):
        self.basis = np.array(basis)
        self.step = step
        self.origin = np.array(origin)
        self.segments = list(segments)

    @classmethod
    def from_points(cls, basis, step, points, tangents=None):
        '''
        Create a Frenet-Serret object from a sequence of points.
        '''
        # XXX This is not typically used, but was meant as a possible way of
        # XXX generating seed paths using rotation minimizing frames.
        curve = cls(basis, step, points[0])
        frames = rmf(points[1:], np.roll(basis, 2, axis=0), tangents=tangents)
        frames = [np.roll(b, -2, axis=0) for b in frames]
        ct = frames_to_ct(frames, step)
        curve.segments = (cls.Segment(k, T) for k, T in ct)
        return curve

    def __eq__(self, o):
        return (isinstance(self, type(o)) and
                np.array_equal(self.basis, o.basis) and
                np.array_equal(self.origin, o.origin) and
                self.step == o.step and
                self.segments == o.segments)

    @classmethod
    def from_dict(cls, d):
        curve = cls(
            basis=d['basis'],
            step=d['step'],
            origin=d['origin'],
            segments=[
                cls.Segment(k, t)
                for (k, t) in d['segments']
            ]
        )
        return curve

    def to_dict(self):
        return {
            'basis': self.basis.tolist(),
            'step': self.step,
            'origin': self.origin.tolist(),
            'segments': [
                (s.curvature, s.torsion)
                for s in self.segments
            ],
        }

    @classmethod
    def get_transition_matrix(cls, k, T, ds):
        '''
        Calculate a transition matrix given a segment's curvature, torsion, and
        step size.

        Parameters:
            k: curvature
            T: torsion
            ds: step size
        '''
        k2 = k ** 2
        T2 = T ** 2
        l2 = k ** 2 + T ** 2
        l = math.sqrt(l2)
        c = math.cos(l * ds)
        s = math.sin(l * ds)
        if l == 0:
            return np.eye(3, 3)
        else:
            return np.array([
                [
                    1 - (k2 / l2) * (1 - c),
                    (k / l) * s,
                    (k * T) / l2 * (1 - c)
                ],
                [-k / l * s, c, T / l * s],
                [
                    (k * T) / l2 * (1 - c),
                    -(T / l) * s,
                    1 - (T2 / l2) * (1 - c)
                ],
            ])

    def bases(self):
        '''
        Iterator over the orthonormal bases.
        '''
        yield self.basis
        f = self.basis
        for segment in self.segments:
            u = segment.transition_matrix
            f = np.dot(u, f)
            yield f

    def points(self, bases=False):
        '''
        Iterator over the points.

        If `bases` is True, yields 2-tuple of (basis, point).
        '''
        x = self.origin
        if bases:
            yield None, x
        else:
            yield x
        for f in self.bases():
            t, n, b = f
            x = x + t * self.step
            if bases:
                yield f, x
            else:
                yield x

    def report(self, indices, bases=False):
        '''
        Report the i-th point, for each i in `indices`.

        If `bases` is True, also report the basis as is done in `points()`.
        '''
        d = {}
        for i, p in enumerate(self.points(bases=bases)):
            d[i] = p
        return [d.get(i, i) for i in indices]

    def end_point(self, basis=False):
        '''
        Calculate the last point.

        If `bases` is True, also report the basis as is done in `points()`.
        '''
        for p in self.points(bases=basis):
            pass
        return p

    def end_basis(self):
        '''
        Calculate the last basis.
        '''
        for b in self.bases():
            pass
        return b

    @property
    def arclength(self):
        '''Arc length of the Frenet-Serret curve.'''
        return self.step * (1 + len(self.segments))

    @property
    def segments(self):
        try:
            return self._segments
        except AttributeError:
            return ()

    @segments.setter
    def segments(self, value):
        '''Forces segments to have a reference to this object'''
        cls = type(self)
        self._segments = tuple(cls.Segment(v.curvature, v.torsion, self)
                               for v in value)
FS = FrenetSerret  # For backwards compatibility


def perturb(
    curve,
    max_iterations=None,
    goal=1,
    x1=None,
    n1=None,
    tol=1e-8,
    callback=None,
    importance=None,
    func=None,
    rng=None,
    **kw
):
    """ Convenience function for perturbing a curve.

    Picks segments according to the algorithm given in the paper and inserts
    suitable defaults for the convergence operation.

    :param FS curve: Frenet-Serret curve to perturb
    :param max_iterations: setting this to an integer guarantees that no more
                           than this many root-finding attempts will occur
    :param goal: number of perturbations to attempt
    :param array x1: End point to try and converge to. If None, tries to use
                     the end point of the input curve.
    :param array n1: End tangent vector to converge to. If None, uses the end
                     tangent of the input curve.
    :param callable callback: Callback function. If defined, is called with two
                              arguments: the iteration id and the number of
                              successful perturbations as judged by the
                              function parameters.
    :param callable importance: A function which, given a curve, returns a
                                probability in range 0..1 that the curve should
                                be accepted. If empty, the probability is
                                assumed to be 1.0.
    :param callable func: Function which chooses the new value of the segment
                          to perturb. A 1-arg callable whose input value is the
                          curvature of the chosen segment. If None, will add a
                          uniform random value in the range [-1, 1] to the
                          input curvature.
    :param Random rng: Instance of ``random.Random`` to use for random number
                       generation. If None, uses the ``random`` module directly
    :param kw: Arguments passed on to ``solve_constraints``.
    :returns: list of convergence result used by scipy.
    """
    rng = rng or random
    func = func or (lambda s: s + rng.uniform(-1, 1))
    importance = importance or (lambda curve: 1.0)
    assert len(curve.segments) >= 3, "must have at least 3 segments"
    x1 = x1 if x1 is not None else curve.end_point()
    n1 = n1 if n1 is not None else curve.end_basis()[0]
    convergences = 0
    i = 0
    result = None
    results = []
    norm = np.linalg.norm
    while convergences < goal and not (max_iterations and i >= max_iterations):
        i1 = rng.randint(1, len(curve.segments) - 2)
        i0 = rng.randint(0, i1 - 1)
        i2 = rng.randint(i1 + 1, len(curve.segments) - 1)
        s0 = curve.segments[i0]
        s1 = curve.segments[i1]
        s2 = curve.segments[i2]
        original_segments = (
            s0.curvature, s0.torsion,
            s1.curvature, s1.torsion,
            s2.curvature, s2.torsion,
        )
        s1.curvature = max(0, func(s1.curvature))
        i += 1
        t0 = time.clock()
        result = solve_constraints(curve, (i0, i1, i2), x1, n1, tol=tol, **kw)
        t1 = time.clock()
        result.time = t1 - t0
        result.importance = None
        results.append(result)
        # FIXME: ideally we should only have to check the "success" flag, but
        # SciPy will regardless converge on wrong answers with it set.
        if (
            result.success and
            norm(result.fun[:3], 2) < tol and
            result.fun[-1] < tol
        ):
            (s0.curvature,
             s2.curvature,
             s0.torsion,
             s1.torsion,
             s2.torsion) = result.x
            result.importance = importance(curve)
            result.accepted = (rng.uniform(0, 1) < result.importance)
            if result.accepted:
                convergences += 1
            else:
                (
                    s0.curvature, s0.torsion,
                    s1.curvature, s1.torsion,
                    s2.curvature, s2.torsion,
                ) = original_segments
        else:
            result.accepted = False
            s1.curvature = original_segments[2]
        if callback:
            callback(iter=i, convergences=convergences, indices=(i0, i1, i2))
    return results


def solve_constraints(curve, indices, x1, n1, **kw):
    i0, i1, i2 = indices
    s0, s1, s2 = [curve.segments[i] for i in indices]
    original_x = (s0.curvature,
                  s2.curvature,
                  s0.torsion,
                  s1.torsion,
                  s2.torsion)

    # partially evaluate the chain of matrices
    D = [s.transition_matrix for s in curve.segments[:i0]]
    D = reduce(np.dot, reversed(D), np.eye(3))

    C = [s.transition_matrix for s in curve.segments[i0 + 1:i1]]
    C = reduce(np.dot, reversed(C), np.eye(3))

    B = [s.transition_matrix for s in curve.segments[i1 + 1:i2]]
    B = reduce(np.dot, reversed(B), np.eye(3))

    A = [s.transition_matrix for s in curve.segments[i2 + 1:]]
    A = reduce(np.dot, reversed(A), np.eye(3))

    # p0 is the point just before segment 0 and p00 is the point just after
    # and so forth
    (p0, p00, p1, p11, p2, p22) = curve.report((i0 + 1, i0 + 2,
                                                i1 + 1, i1 + 2,
                                                i2 + 1, i2 + 2))
    p_final = curve.end_point()

    u0 = s0.transition_matrix
    u1 = s1.transition_matrix
    u2 = s2.transition_matrix
    s0_basis = u0.dot(D).dot(curve.basis)
    s1_basis = u1.dot(C).dot(s0_basis)
    s2_basis = u2.dot(B).dot(s1_basis)

    # we leave v0 in world space since its basis (the curve's starting basis)
    # will never change by way of the solver
    v0 = p0 - curve.origin

    # note v1, v2, and v3 are in terms of their respective bases
    v1 = np.dot(s0_basis, p1 - p00)
    v2 = np.dot(s1_basis, p2 - p11)
    v3 = np.dot(s2_basis, p_final - p22)

    # pull in inv() for convenience
    inv = np.linalg.inv

    def _fn(x):
        s0.curvature = x[0]
        s2.curvature = x[1]
        s0.torsion = x[2]
        s1.torsion = x[3]
        s2.torsion = x[4]

        # compute bases
        u0 = s0.transition_matrix
        u1 = s1.transition_matrix
        u2 = s2.transition_matrix
        s0_basis = u0.dot(D).dot(curve.basis)
        s1_basis = u1.dot(C).dot(s0_basis)
        s2_basis = u2.dot(B).dot(s1_basis)
        end_basis = A.dot(s2_basis)

        # compute end point
        end_point = (curve.origin + v0 +
                     np.dot(inv(s0_basis), v1) + s0_basis[0] * curve.step +
                     np.dot(inv(s1_basis), v2) + s1_basis[0] * curve.step +
                     np.dot(inv(s2_basis), v3) + s2_basis[0] * curve.step)

        # compute differences
        dn = end_basis[0] - n1
        dx = end_point - x1

        return [dx[0], dx[1], dx[2], sum(dn), sum(dn ** 2)]

    result = root(_fn, original_x, **kw)
    (s0.curvature,
     s2.curvature,
     s0.torsion,
     s1.torsion,
     s2.torsion) = original_x

    return result


def estimate_tangents(points):
    """Estimate the unit tangents between a set of points.

    For the endpoints, this is just the normalized difference between the
    first/second and last/next-to-last points. For the internal points, the
    neighboring unit tangents are averaged.

    :param points: list of 2 or more points
    :returns: approximated tangent vectors at each point
    """
    from .gfx import normalize
    if len(points) < 2:
        raise ValueError('At least two points are required.')
    dx = [
        normalize(x2 - x1)
        for x1, x2 in zip(points, points[1:])
    ]
    tangents = []
    tangents.append(dx[0])
    for u1, u2 in zip(dx, dx[1:]):
        tangents.append(normalize(0.5 * (u1 + u2)))
    tangents.append(dx[-1])
    return tangents


def rmf(points, basis, tangents=None):
    """ Compute rotation minimizing frames for a set of points.

    This implements the double-reflection algorithm for computing the rotation
    minimizing frame (Wang et. al. 2008). Given a set of points, an initial
    basis and optionally the tangents for the points, it computes a set of
    orthonormal frames which minimizes rotation about the instantaneous
    tangent.

    :param list points: a list of points in 3d space
    :param ndarray basis: a 3x3 starting orthonormal frame, in the order of
                          normal, binormal, and tangent
    :param list tangents: a list of tangents for the points
    :returns list: the rotation minimizing orthonormal frames, arranged in the
                   order of normal, binormal, and tangent

    If `tangents` is undefined, they will be estimated by
    :fun:`estimate_tangents`.

    This function deviates from the usual ordering of orthonormal frames
    (tangent, normal, binormal) since this implementation adopts the convention
    of the original paper.
    """
    from numpy import dot
    from numpy.testing import assert_almost_equal
    points = np.array(points)

    # Estimate tangents as needed. We ignore the estimate for the first tangent
    # since the caller has to provide that.
    if tangents is None:
        tangents = estimate_tangents(points)
        tangents[0] = basis[2]

    # sanity check
    assert len(points) == len(tangents)
    assert_almost_equal(tangents[0], basis[2])

    # allocate memory for return value
    bases = [None] * len(points)
    bases[0] = basis

    for i in xrange(len(points) - 1):
        # setup for math-friendly variable names
        ri, si, ti = bases[i]
        ti1 = tangents[i + 1]

        # main algorithm
        v1 = points[i + 1] - points[i]
        c1 = dot(v1, v1)
        r_L = ri - (2 / c1) * dot(v1, ri) * v1
        t_L = ti - (2 / c1) * dot(v1, ti) * v1
        v2 = ti1 - t_L
        c2 = dot(v2, v2)
        ri1 = r_L - (2 / c2) * dot(v2, r_L) * v2
        si1 = np.cross(ti1, ri1)
        bases[i + 1] = np.array([ri1, si1, ti1])
    return bases


def frames_to_ct(frames, ds):
    """
    Convert a sequence of frames (tangent, normal, binormal) to curvatures and
    torsions.
    """
    from numpy.linalg import norm
    from numpy import dot
    return [
        (
            norm((f2[0] - f1[0]) / ds, 2),  # definition of curvature
            dot(-f1[1], (f2[2] - f1[2]) / ds),  # definition of torsion
        )
        for f1, f2 in zip(frames, frames[1:])
    ]

"""
Convenient OpenGL drawing routines.

Primarily used by twisty-show.py
"""
import OpenGL.GL as gl
import OpenGL.GLU as glu
import math
import numpy as np

from .fs import FS


def draw_curve(curve):
    """ Draw a curve or otherwise sequence of points.
    """
    if isinstance(curve, FS):
        draw_curve(curve.points())
    gl.glBegin(gl.GL_LINE_STRIP)
    for p in curve:
        gl.glVertex3f(*p)
    gl.glEnd()


def draw_grid(m, n):
    """ Draw a m by n grid on the y=0 plane.
    """
    x0 = int(-m / 2)
    z0 = int(-n / 2)
    gl.glBegin(gl.GL_QUADS)
    for x in range(m):
        for z in range(n):
            gl.glVertex3f(x0 + x, 0, z0 + z)
            gl.glVertex3f(x0 + x + 1, 0, z0 + z)
            gl.glVertex3f(x0 + x + 1, 0, z0 + z + 1)
            gl.glVertex3f(x0 + x, 0, z0 + z + 1)
    gl.glEnd()


def draw_histogram(
    histogram, axes_color=(1, 0, 0, 1), plot_color=(1, 1, 1, 1),
    logscale=None, tickx=0.1,
):
    """ Draw a histogram.

    The histogram will draw on an orthographic projection on the bounds [0..1]
    where a unit is 1/20th of the viewport size.
    """
    if histogram.empty:
        return

    # extract lower and upper corners
    llc = np.array(histogram.llc)
    urc = np.array(histogram.urc)

    # log-scale y-components
    if logscale is not None:
        llc[1] = 0.0
        urc[1] = math.log(urc[1], logscale) if urc[1] > 0 else 0

    d = urc - llc
    if not d.all():
        return

    # use an orthographic projection
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glPushMatrix()
    gl.glLoadIdentity()
    glu.gluOrtho2D(-10, 10, -10, 10)

    def tick(x=None, y=None, nx=None, ny=None):
        assert any(v is not None for v in (x, nx))
        assert any(v is not None for v in (y, ny))
        nx = nx if nx is not None else (x - llc[0]) / d[0]
        ny = ny if ny is not None else (y - llc[1]) / d[1]
        assert 1 >= nx >= 0, "X (%f) not normalized on %s" % (
            x, [llc[0], urc[0]])
        assert 1 >= ny >= 0, "Y (%f) not normalized on %s" % (
            y, [llc[1], urc[1]])
        gl.glVertex3f(nx, ny, 0)

    # draw the plot lines
    gl.glColor(*plot_color)
    gl.glBegin(gl.GL_LINE_STRIP)
    for x, y in histogram.walk():
        # logscale if necessary
        if logscale is not None:
            y = math.log(y) / math.log(logscale) if y > 0 else 0
        tick(x=x, y=y)
    gl.glEnd()

    # draw the axes
    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glColor(*axes_color)
    gl.glBegin(gl.GL_LINES)

    # x = 0
    tick(nx=0, ny=0)
    tick(nx=1, ny=0)

    # y = 0
    if llc[0] <= 0 and urc[0] > 0:
        tick(x=0, ny=0)
        tick(x=0, ny=1)

    # ticks along x axis
    minx = llc[0]
    maxx = urc[0]
    ticks = np.arange(0, max(0, maxx), tickx)
    ticks = np.append(ticks, np.arange(0, min(0, minx), -tickx))
    ticks = np.array([t for t in ticks if minx <= t <= maxx])
    for i in ticks:
        tick(x=i, ny=0)
        tick(x=i, ny=0.05)
    gl.glEnd()

    # restore the projection matrix
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glPopMatrix()
    gl.glMatrixMode(gl.GL_MODELVIEW)

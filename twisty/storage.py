from abc import abstractmethod
from collections import OrderedDict
import json
import errno
import os
import logging
import sys

from . import config


class Record(object):
    '''
    Flexible record object. Stores keyword-arguments as attributes on the
    object.
    '''

    def __init__(self, **kw):
        for k, v in kw.iteritems():
            setattr(self, k, v)
        self._dict = OrderedDict(sorted(kw.items()))

    def _asdict(self):
        return OrderedDict(self._dict)

    def __iter__(self):
        return self._dict.itervalues()


class StorageError(Exception):
    pass


class Base(config.Element):
    '''
    Abstract key-value storage system. Can retrieve and store Record objects by
    their keys.

    '''

    @abstractmethod
    def store(self, key, record):
        """Store a record or overwrite and existing one."""
        raise NotImplementedError

    @abstractmethod
    def read(self, key):
        """ Read an existing record.

        Raises KeyError if the record does not exist.
        """
        raise NotImplementedError


class CSV(Base):
    '''
    Output CSV data to stdout or a file.

    This is a simple form which writes a file and doesn't support lookup
    (generally not required). It always writes the header of the record
    prefixed by a hash mark (#).

    Parameters
    ----------

    output : string or None
        If None, write to stdout. If a string, write data to a file at the
        given path.
    '''

    def __init__(self, output=None):
        if isinstance(output, basestring):
            self.path = output
            self._fp = open(output, 'w')
        else:
            self.path = None
            self._fp = output if output is not None else sys.stdout

    def __del__(self):
        if self.path is not None:
            self._fp.close()

    def read(self, key):
        raise NotImplementedError

    def store(self, key, record):
        keys = record._asdict().keys()
        line = ','.join(keys)
        self._fp.write('# ' + line + '\n')
        line = ','.join([str(f) for f in record])
        self._fp.write(line + '\n')

    def to_dict(self):
        return {
            'type': self.get_type(),
            'path': self.path,
        }


class Composite(Base):
    '''
    Composite storage system. Useful if a storage system might intermittently
    fail.

    Parameters
    ----------

    backends : dict
        A dict of ``storage.Base`` objects to attempt to store to. Storage is
        attempted in the lexicographic order of their dict keys.

    '''

    def __init__(self, backends):
        self.backends = dict(backends)
        self.order = sorted(self.backends.keys())

    @classmethod
    def from_dict(cls, d):
        d['backends'] = dict(
            (name, Base.create(backend))
            for (name, backend) in d['backends'].iteritems()
        )
        return cls(**d)

    def store(self, key, record):
        for backend_key in self.order:
            backend = self.backends[backend_key]
            try:
                backend.store(key, record)
                return
            except:
                logging.warn('Storing to backend "%s"', backend_key,
                             exc_info=1)
        raise StorageError('No storage backends could store the record.')

    def read(self, key):
        for backend_key in self.order:
            backend = self.backends[backend_key]
            try:
                return backend.read(key)
            except:
                logging.warn('Reading from backend "%s"', backend_key,
                             exc_info=1)
        raise KeyError(key)

    def to_dict(self):
        return {
            'type': self.get_type(),
            'backends': dict(
                (k, v.to_dict())
                for (k, v) in self.backends.iteritems()
            )
        }


class Local(Base):
    '''
    Output JSON data to a local file system, stored in a hierarchical directory
    structure.

    This is an OK way of doing things if you don't mind the data being stored
    as JSON. CSV is generally more useful.

    Parameters
    ----------

    root : str
        An absolute path to the root directory where data will be stored.

    '''

    def __init__(self, root):
        self.root = root

    def store(self, key, record):
        path = self._to_path(key)
        with open(path, 'w') as fp:
            fp.write(json.dumps(record._asdict()))

    def read(self, key):
        path = self._to_path(key, create=False)
        try:
            with open(path) as fp:
                d = json.load(fp)
                assert isinstance(d, dict), "Not a JSON object (key: %s)" % key
                return Record(**d)
        except IOError as e:
            if e.errno != errno.ENOENT:
                raise
            raise KeyError(key)

    def to_dict(self):
        return {
            'type': self.get_type(),
            'root': self.root,
        }

    def _to_path(self, key, create=True):
        p = [key[i:i + 2] for i in range(0, 8, 2)]
        p.extend(key[i:i + 4] for i in range(8, 20, 4))
        p.append(key[20:])
        p = os.path.join(self.root, *p)
        try:
            if create:
                os.makedirs(os.path.dirname(p))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        return p

'''
Path segment weight calculation.

'''
import scipy.integrate
import math
import numpy as np
from abc import abstractmethod

from .util import _wrap_constant, PrettyPartial
from . import config


def generator(fn):
    """ Decorator which sets a ``make`` function.

    A decorated function can then be generated like so:

      newfunc = fn.make(0.1)
    """
    fn.make = lambda mu: PrettyPartial(fn, mu=mu)
    return fn


@generator
def simple_gaussian_phase(p, mu):
    """Parameterized simple Gaussian phase function.

    Parameters:
      p -- (float) location of evaluation
      mu - (float) mean of Gaussian
    """
    return math.exp((-mu * p ** 2) / 2)


@generator
def gaussian_phase(p, mu):
    """Parameterized Gaussian phase function.

    Parameters:
      p -- (float) location of evaluation
      mu -- (float) mean of Gaussian
    """
    return (math.sqrt(math.pi * mu / 2)
            * simple_gaussian_phase(p, mu)
            / (1 - math.exp(-2 / mu)))


def sv_integrate_quad(integrand, a, b):
    """Integrate a function using ``scipy.integrate.quad``.

    Returns result of integration.

    Parameters:
      integrand - (callable) function to integrate
      a - (float) lower bound of integration
      b - (float) upper bound of integration
    """
    y, _ = scipy.integrate.quad(integrand, a, b)
    return y


def sv_integrate_simple(integrand, a, b, step=None):
    """Integrate a function using a simplistic quad-like method.

    Parameters:
      integrand - (callable) function to integrate
      a - (float) lower bound of integration
      b - (float) upper bound of integration
    """
    if step is None:
        step = (b - a) / 2000
    y = 0.0
    y1 = integrand(a)
    for p in np.arange(a + step, b, step):
        y2 = integrand(p)
        y += step * (y1 + y2) / 2
        y1 = y2
    return y


def weight_path(curve, Af, Bf, strategy, clip=None):
    """Evaluate the weight of a single path.

    Parameters:
      curve -- (FS) a discrete Frenet-Serret curve
      Af -- (callable) an absorption function or constant
      Bf -- (callable) a density function or constant
      strategy -- (IntegralStrategy) integration scheme
    """
    w = 1.0
    for weight in weight_nodes(curve, Af, Bf, strategy):
        w *= weight
    return w


def weight_nodes(curve, Af, Bf, strategy, clip=None):
    """Evaluate the weight of the nodes of a path.

    Parameters:
      curve -- (FS) a discrete Frenet-Serret curve
      Af -- (callable) an absorption function or constant
      Bf -- (callable) a density function or constant
      strategy -- (IntegralStrategy) integration scheme
      clip -- (2-tuple or None) minimum and maximum bounds for clipping

    If no clip parameter is provided, by default the weights will be clipped on
    (0, inf).
    """
    clip = clip or (0, None)
    Af = _wrap_constant(Af)
    Bf = _wrap_constant(Bf)
    ds = curve.step
    for s, x in zip(curve.segments, curve.points()):
        a = Af(x)
        b = Bf(x)
        k = s.curvature
        wt = strategy.eval(b, a, k, ds)
        if not clip[0] is None:
            wt = max(clip[0], wt)
        if not clip[1] is None:
            wt = min(clip[1], wt)
        yield wt


class IntegralStrategy(config.Element):

    '''
    A strategy for evaluating the path segment weight function. Subclasses must
    define `integrate()`.

    '''

    def eval(self, b, a, k, ds):
        """Calculate the weight function for a node."""
        c = a + b
        constant = math.exp(-c * ds) / (2 * math.pi ** 2)
        return constant * self.integrate(b, k, ds)

    @abstractmethod
    def integrate(self, b, k, ds):
        """Perform integration strategy.

        Parameters:
          b -- (float) the density
          k -- (float) the curvature
          ds -- (float) the step size
        """
        raise NotImplementedError()


class RegularizedIntegral(IntegralStrategy):
    '''
    Numerically evaluate path weight integral, introducing an epsilon term to
    make the integration numerically feasible.

    Parameters
    ----------

    mu : float
        Gaussian phase function width parameter.
    step : float
        Step size used for integration.
    bounds : 2-tuple or None
        Minimum and maximum of interval to step over when evaluating the
        integral.
    eps : float
        A small value. The smaller this value, the more accurate the
        integration, but breaks down numerically if the value is too small.
    '''

    def __init__(self, mu=0.1, step=0.01, bounds=None, eps=0.0003):
        super(RegularizedIntegral, self).__init__()
        self.mu = mu
        self.phase = PrettyPartial(gaussian_phase, mu=mu)
        self.eps = eps
        self.step = step
        self.method = PrettyPartial(sv_integrate_simple, step=step)
        self.bounds = bounds

    def to_dict(self):
        return {
            'type': self.get_type(),
            'mu': self.mu,
            'eps': self.eps,
            'step': self.step,
            'bounds': self.bounds,
        }

    def integrate(self, b, k, ds):
        """Compute numerical approximation of regularized integral.

        Parameters:
          b -- (float) the density
          k -- (float) the curvature
          ds -- (float) the step size
        """
        bounds = self.bounds or (0, 10 / self.eps)
        kds = k * ds
        bds = b * ds
        integrand1 = lambda p: self._integrand(p, kds, bds)
        integrand2 = lambda p: self._integrand(p, 0, bds)
        return self.method(integrand1, *bounds) / \
            self.method(integrand2, *bounds)

    def integrand(self, p, b, k, ds):
        """Integrand portion for regularized integration.

        Parameters:
          p -- (float) integration parameter
          b -- (float) the density
          k -- (float) the curvature
          ds -- (float) the step size
        """
        kds = k * ds
        bds = b * ds
        return self._integrand(p, kds, bds)

    def _integrand(self, p, kds, bds):
        return (
            p * math.exp(bds * self.phase(p) - (self.eps ** 2) / 2 * p ** 2)
            * (math.sin(kds * p) / kds if kds != 0 else p)
        )


class CachedIntegral(IntegralStrategy):

    '''
    Cache another IntegralStrategy object, and interpolate over (kappa * ds) x
    (b * ds) to find in-between values.

    Parameters
    ----------

    cached_integral : IntegralStrategy
        The IntegeralStrategy object used to back the cache.
    size : float
        The size of the 2d grid used for the cache. Units are in sizes of
        (kappa * ds) and (b * ds).
    '''

    def __init__(self, cached_integral, size=0.01):
        super(CachedIntegral, self).__init__()
        self._cached_integral = cached_integral
        self._size = size
        self._grid = {}

    @classmethod
    def from_dict(cls, d):
        cached_integral = IntegralStrategy.create(d.pop('cached_integral'))
        return cls(cached_integral=cached_integral, **d)

    def to_dict(self):
        return {
            'type': self.get_type(),
            'size': self._size,
            'cached_integral': self._cached_integral.to_dict(),
        }

    def integrate(self, b, k, ds):
        """Attempt to look up a value before evaluating the cached integral.

        In-between values are resolved via bilinear interpolation.

        Parameters:
          b -- (float) the density
          k -- (float) the curvature
          ds -- (float) the step size
        """
        bds = b * ds
        kds = k * ds
        fi = bds / self._size
        fj = kds / self._size
        i = int(math.floor(fi))
        j = int(math.floor(fj))
        wi = 1 - (fi - i)
        wj = 1 - (fj - j)

        value = 0.0

        w = wi * wj
        if w > 0:
            value += w * self._eval_grid(i, j, ds)

        w = wi * (1 - wj)
        if w > 0:
            value += w * self._eval_grid(i, j + 1, ds)

        w = (1 - wi) * wj
        if w > 0:
            value += w * self._eval_grid(i + 1, j, ds)

        w = (1 - wi) * (1 - wj)
        if w > 0:
            value += w * self._eval_grid(i + 1, j + 1, ds)

        return value

    def _eval_grid(self, i, j, ds):
        ij = (i, j)
        value = self._grid.get(ij)
        if value is not None:
            return value
        bds = i * self._size
        kds = j * self._size
        b = bds / ds
        k = kds / ds
        value = self._cached_integral.integrate(b, k, ds)
        self._grid[ij] = value
        return value

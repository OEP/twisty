"""
Various methods to create seed paths.

"""

import numpy as np
from scipy import optimize

from . import config, fs
from .gfx import uniform_sphere
from .geometry import Geometry
from .parametric import Bezier

from abc import abstractmethod
import random


def create_curve(spec, project=0.5, project_max=1e8, rng=None, l0=None,
                 l1=None, n2=None, arclength_goal=None):
    '''
    Create a 5-degree Bezier curve matching the constraints given from
    ``spec``.

    Parameters:
        spec -- (CurveSpec) a specification for the curve to be created
        project -- (float) fraction of the distance |spec.x1 - spec.x0| to use
            in placement of second and fourth control vertices (`l1` and `l0`
            in the code below).
        project_max -- (float) when solving for the arc length, a maximum
            distance
        rng -- (Random) a random number generator
        l0 -- (float) distance to trace along `spec.n0`, by default it is
            randomized
        l1 -- (float) distance to trace along `spec.n1`, by default it is
            randomized
        n2 -- (array) unit vector to trace along from (spec.x0 + spec.x1)/2 to
            find the third control vertex, defaults to spherical uniform random
            vector
        arclength_goal -- (float) desired arc length for the curve

    '''
    if rng is None:
        rng = random
    if n2 is None:
        n2 = uniform_sphere(rng)
    else:
        n2 = np.asarray(n2)

    pathlength = np.linalg.norm(spec.x1 - spec.x0)
    if l0 is None:
        l0 = rng.uniform(0, project * pathlength)
    if l1 is None:
        l1 = rng.uniform(0, project * pathlength)

    # Set up the initial control vertices. These will be modified in-place by
    # the optimization routines later, so for now we ignore the third control
    # vertex.
    xmid = (spec.x0 + spec.x1) / 2
    control = [
        spec.x0,
        spec.x0 + l0 * spec.n0,
        xmid,
        spec.x1 - l1 * spec.n1,
        spec.x1,
    ]
    bezier = Bezier(length=1., control=control)

    # For convenience, gives the arc length of the Bezier curve if `l2` were
    # changed.
    def arclength(l2):
        original_p2 = bezier.control[2]
        bezier.control[2] = xmid + l2 * n2
        result = bezier.arclength(bezier.length)
        bezier.control[2] = original_p2
        return result

    # Get sane values for the minimum and maximum arc length.
    arclength_min = max(arclength(0), spec.arclength[0])
    arclength_max = min(arclength(project_max), spec.arclength[1])
    if arclength_max <= arclength_min:
        raise CreateFailed('Arclength spec too restrictive')

    if arclength_goal is None:
        arclength_goal = rng.uniform(arclength_min, arclength_max)

    def func(l2):
        return arclength(l2) - arclength_goal

    # Now we can optimize l2 to get the desired arc length.
    l2 = optimize.brentq(func, 0, project_max)
    bezier.control[2] = xmid + l2 * n2
    return bezier


class CurveSpec(object):
    '''
    A bundle of space curve constraints. They are:

        * x0: the space curve starting position
        * n0: the space curve starting direction
        * x0: the space curve ending position
        * n0: the space curve ending direction
        * arclength: a range (2-tuple) of permissible arclength values. If a
            single value is given, the curve generated will have that exact arc
            length.

    '''

    def __init__(self, x0, n0, x1, n1, arclength):
        self.x0 = np.asarray(x0, dtype=float)
        self.n0 = np.asarray(n0, dtype=float)
        self.x1 = np.asarray(x1, dtype=float)
        self.n1 = np.asarray(n1, dtype=float)
        self.arclength = _force_range(arclength)


class CreateFailed(Exception):
    '''
    Raised when creating a curve is not possible.

    '''
    pass


class Constraints(object):

    """
    A constraints set for for an experiment. This is returned after

    #

    Parameters:
      curve: a Frenet-Serret curve
      x1: the ending point
      n1: the ending tangent
      camera: the scene camera, if any
    """
    # XXX This class and CurveSpec could probably be rolled together.
    # XXX This notion of a Camera is kind of dead since it doesn't always
    # translate for, say, a spherically-shaped sensor. It's much easier to
    # assign such coordinate systems during analysis.

    def __init__(self, curve, x1=None, n1=None, camera=None, meta=None):
        self.curve = curve
        self.meta = {}
        if x1 is None:
            x1 = list(self.curve.points())[-1]
        if n1 is None:
            n1 = list(self.curve.bases())[-1][0]
        if meta is not None:
            self.meta.update(meta)
        self.x1 = x1
        self.n1 = n1
        self.camera = camera


class BootstrapStrategy(config.Element):
    '''
    Abstract base class for a seed path generation strategy.
    '''

    @abstractmethod
    def create_curve(self, rng):
        """ Called once to create the curve.
        """
        raise NotImplementedError


class _PartialCurve(BootstrapStrategy):
    """
    Seed path generation methods which involve constructing the Frenet-Serret
    object directly may wish to inherit from this class. See examples of its
    usage directly below.

    """

    def __init__(self, segments, step, origin=None, basis=None):
        self.segments = segments
        self.step = step
        self.origin = origin
        self.basis = basis

    @abstractmethod
    def _get_kwargs(self, rng):
        raise NotImplementedError

    @abstractmethod
    def _get_dict(self):
        raise NotImplementedError

    def create_curve(self, rng):
        kwargs = self._get_kwargs(rng)
        kwargs.setdefault('step', self.step)
        kwargs.setdefault('origin', self.origin or np.zeros(3))
        kwargs.setdefault('basis', self.basis or np.eye(3))
        curve = fs.FS(**kwargs)
        return Constraints(curve=curve)

    def to_dict(self):
        d = self._get_dict()
        d.setdefault('type', self.get_type())
        d.setdefault('step', self.step)
        d.setdefault('origin', self.origin)
        d.setdefault('base', self.base)
        return d


class ConstantHelicity(_PartialCurve):

    """ Creates a FS curve with constant curvature and torsion.
    """

    def __init__(self, curvature, torsion, **kw):
        super(ConstantHelicity, self).__init__(**kw)
        self.curvature = curvature
        self.torsion = torsion

    def _get_kwargs(self, rng):
        return {
            'segments': [
                fs.FS.Segment(self.curvature, self.torsion)
                for _ in xrange(self.segments)
            ]
        }

    def _get_dict(self):
        return {
            'curvature': self.curvature,
            'torsion': self.torsion,
        }


class Random(_PartialCurve):

    """ Creates a FS curve with constant random and torsion.
    """

    def __init__(self, curvature_max, torsion_max, **kw):
        super(Random, self).__init__(**kw)
        self.curvature_max = curvature_max
        self.torsion_max = torsion_max

    def _get_kwargs(self, rng):
        return {
            'segments': [
                fs.FS.Segment(
                    rng.uniform(0, self.curvature_max),
                    rng.uniform(0, self.torsion_max),
                )
                for _ in xrange(self.segments)
            ]
        }

    def _get_dict(self):
        return {
            'curvature_max': self.curvature_max,
            'torsion_max': self.torsion_max,
        }


class SimpleGeometry(BootstrapStrategy):
    '''
    Create space curves which are based on emitter and sensor geometries. See
    the `twisty.geometry` module for some examples of geometries.

    '''

    def __init__(self, emitter, sensor, resolution, arclength):
        self.emitter = emitter
        self.sensor = sensor
        self.resolution = resolution
        self.arclength = _force_range(arclength)

    @classmethod
    def from_dict(cls, d):
        d['emitter'] = Geometry.create(d.pop('emitter'))
        d['sensor'] = Geometry.create(d.pop('sensor'))
        return cls(**d)

    def create_curve(self, rng, noop=False):
        x0, n0 = self.emitter.choose(rng)
        x1, n1 = self.sensor.choose(rng)

        spec = CurveSpec(x0=x0, n0=n0, x1=x1, n1=n1, arclength=self.arclength)
        bezier = create_curve(spec, rng=rng)
        if not noop:
            curve = bezier.to_fs(self.resolution)
        else:
            curve = None
        meta = {
            'parametric': bezier,
        }
        return Constraints(curve, x1=x1, n1=n1, meta=meta)

    def to_dict(self):
        return {
            'sensor': self.sensor.to_dict(),
            'emitter': self.emitter.to_dict(),
            'resolution': self.resolution,
            'arclength': self.arclength,
        }


def _force_range(arg):
    '''
    Transform given arg into a 2-tuple if it is an int or float.
    '''
    if isinstance(arg, (int, float)):
        return (arg, arg)
    else:
        return arg

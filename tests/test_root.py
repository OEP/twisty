from testlib import BaseTest, XYZ_BASIS
from twisty.fs import solve_constraints, FS
import numpy as np
from numpy.linalg import norm
import numpy.testing as nt
from random import choice, uniform


class RootTest(BaseTest):

    def setUp(self):
        self.curve = FS(XYZ_BASIS, 0.1)
        self.curve.segments = [FS.Segment(1, 1) for i in range(500)]
        self.points = list(self.curve.points())
        self.normals = [(p1 - p0) / norm(p1 - p0, 2)
                        for p0, p1 in zip(self.points, self.points[1:])]

    def test_unperturbed_convergence(self):
        e = self.curve.end_point()
        t = self.curve.end_basis()[0]

        result = solve_constraints(self.curve, (2, 3, 4),
                                   e, t)
        self.assertTrue(result.success, result.message)
        nt.assert_almost_equal([1] * 5, result.x)
        e1 = self.curve.end_point()
        t1 = self.curve.end_basis()[0]
        nt.assert_almost_equal(e, e1)
        nt.assert_almost_equal(t, t1)

    def test_perturbed_convergence(self):
        e = self.curve.end_point()
        t = self.curve.end_basis()[0]

        s0 = self.curve.segments[2]
        s1 = self.curve.segments[3]
        s2 = self.curve.segments[4]
        s0.curvature = .8

        result = solve_constraints(self.curve, (2, 3, 4),
                                   e, t)
        self.assertTrue(result.success, result.message)
        (s0.curvature,
         s2.curvature,
         s0.torsion,
         s1.torsion,
         s2.torsion) = result.x

        e1 = self.curve.end_point()
        t1 = self.curve.end_basis()[0]
        nt.assert_almost_equal(e, e1, decimal=1)
        nt.assert_almost_equal(t, t1, decimal=1)

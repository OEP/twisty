from testlib import BaseTest
from twisty.fs import FS
import numpy as np
import numpy.testing as npt


class SegmentTest(BaseTest):

    def setUp(self):
        self.curve = FS(np.eye(3), 0.08)
        self.curve.segments = [FS.Segment(0.3, 0.3) for i in range(100)]

    def test_parent_curve(self):
        # parent gets set for assigned nodes
        for segment in self.curve.segments:
            self.assertIs(self.curve, segment.curve, segment)

    def test_cache(self):
        for segment in self.curve.segments:
            # cache computed correctly
            u = segment.transition_matrix
            expected = FS.get_transition_matrix(segment.curvature,
                                                segment.torsion,
                                                self.curve.step)
            npt.assert_almost_equal(expected, u)

            # cache changes with changing curvature and torsion
            original_curvature = segment.curvature
            segment.curvature += 1.2
            expected = FS.get_transition_matrix(segment.curvature,
                                                segment.torsion,
                                                self.curve.step)
            npt.assert_almost_equal(expected, segment.transition_matrix)
            original_torsion = segment.torsion
            segment.torsion += 1.2
            expected = FS.get_transition_matrix(segment.curvature,
                                                segment.torsion,
                                                self.curve.step)
            npt.assert_almost_equal(expected, segment.transition_matrix)

            # multiple deletes ok
            del segment.transition_matrix
            del segment.transition_matrix

    def test_tuple_conversion(self):
        for s in self.curve.segments:
            tup = tuple(s)
            expected = (s.curvature, s.torsion)
            self.assertEqual(expected, tup)

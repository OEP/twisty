from unittest import TestCase
import numpy as np

UX = np.array((1, 0, 0), dtype=np.float)
UY = np.array((0, 1, 0), dtype=np.float)
UZ = np.array((0, 0, 1), dtype=np.float)

XYZ_BASIS = np.array([UX, UY, UZ])
YZX_BASIS = np.array([UY, UZ, UX])
ZXY_BASIS = np.array([UZ, UX, UY])


class BaseTest(TestCase):
    pass

from twisty import geometry
from twisty.gfx import unit

import numpy as np
import numpy.testing as npt

import unittest
import random


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        self.rng = random.Random(0)


class RayTestCase(BaseTestCase):

    def setUp(self):
        super(RayTestCase, self).setUp()
        self.ray = geometry.Ray((0, 0, 0), (1, 0, 0))

    def test_origin1(self):
        npt.assert_almost_equal((0, 0, 0), self.ray.origin)

    def test_direction1(self):
        npt.assert_almost_equal((1, 0, 0), self.ray.direction)

    def test_choose1(self):
        result = self.ray.choose(self.rng)
        expected = (
            (0, 0, 0),
            (1, 0, 0),
        )
        npt.assert_almost_equal(expected, result)


class SphereTestCase(BaseTestCase):

    def setUp(self):
        super(SphereTestCase, self).setUp()
        self.sphere = geometry.Sphere(2, (1, 0, 0))

    def test_center1(self):
        npt.assert_almost_equal((1, 0, 0), self.sphere.center)

    def test_radius1(self):
        self.assertEqual(2, self.sphere.radius)

    def test_choose1(self):
        '''Picks points `radius` units away from center'''
        point, _ = self.sphere.choose(self.rng)
        self.assertEqual((3,), point.shape)
        expected = self.sphere.radius
        result = np.linalg.norm(point - self.sphere.center)
        npt.assert_almost_equal(expected, result)

    def test_choose2(self):
        '''Direction is always in the direction of chosen point'''
        point, direction = self.sphere.choose(self.rng)
        self.assertEqual((3,), direction.shape)
        expected = unit(point - self.sphere.center)
        result = direction
        npt.assert_almost_equal(expected, result)


class SphereThetaTestCase(BaseTestCase):

    def setUp(self):
        super(SphereThetaTestCase, self).setUp()
        self.sphere = geometry.Sphere(2, (1, 0, 0), uniform_in='theta')

    def test_choose1(self):
        '''Picks points `radius` units away from center'''
        point, _ = self.sphere.choose(self.rng)
        self.assertEqual((3,), point.shape)
        expected = self.sphere.radius
        result = np.linalg.norm(point - self.sphere.center)
        npt.assert_almost_equal(expected, result)

    def test_choose2(self):
        '''Direction is always in the direction of chosen point'''
        point, direction = self.sphere.choose(self.rng)
        self.assertEqual((3,), direction.shape)
        expected = unit(point - self.sphere.center)
        result = direction
        npt.assert_almost_equal(expected, result)

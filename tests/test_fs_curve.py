from twisty.fs import FS
from testlib import BaseTest, XYZ_BASIS, YZX_BASIS, ZXY_BASIS
from itertools import product
import numpy as np
from twisty.gfx import rotate
import math

I = np.eye(3)
UX = I[0]
UY = I[1]
UZ = I[2]

BASES = (
    rotate(XYZ_BASIS, UY, math.pi / 3),
    rotate(XYZ_BASIS, UX + UZ, math.pi),
)

STEPS = (0.1, 2)
SEGMENTS = (200, 50)
GLOBALS = globals()


class ZeroCurveTest(object):

    @classmethod
    def manufacture(cls, basis, step, segments):
        class Test(BaseTest, cls):

            def setUp(self):
                self.curve = FS(basis, step)
                self.curve.segments = [
                    FS.Segment(
                        0,
                        0) for i in range(segments)]
        return Test

    def test_same(self):
        for f in self.curve.bases():
            norm = np.linalg.norm(f - self.curve.basis, 2)
            self.assertTrue(norm < 0.01,
                            "%s != %s (norm of difference: %f)"
                            % (self.curve.basis, f, norm))

    def test_points_length(self):
        result = len(list(self.curve.points()))
        expected = 2 + len(self.curve.segments)
        self.assertEqual(expected, result)

    def test_trace(self):
        t, n, b = self.curve.basis
        ds = self.curve.step
        for i, x in enumerate(self.curve.points()):
            expected = self.curve.origin + i * ds * t
            norm = np.linalg.norm(x - expected, 2)
            self.assertTrue(norm < 1e-6,
                            "%s != %s (norm of difference: %f)"
                            % (x, expected, norm))

    def test_dict(self):
        d = self.curve.to_dict()
        c = FS.from_dict(d)
        self.assertEqual(self.curve, c)

for basis, step, segments in product(BASES, STEPS, SEGMENTS):
    name = 'ZeroCurveTest_{basis}_Step_{step}_Segments_{segments}'.format(
        basis=str(basis),
        step=step,
        segments=segments
    )
    cls = ZeroCurveTest.manufacture(basis, step, segments)
    cls.__name__ = name
    GLOBALS[name] = cls

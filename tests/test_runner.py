from testlib import BaseTest

from twisty.runner import Runner
from twisty.fs import FS

import copy

DEFAULT_CONFIG = {
    'bootstrap': {
        'type': 'twisty.bootstrap.ConstantHelicity',
        'basis': [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
        'curvature': 1,
        'origin': [0, 0, 0],
        'segments': 100,
        'step': 0.01,
        'torsion': 1,
    },
    'weighter': {
        'type': 'twisty.weight.CachedIntegral',
        'cached_integral': {
            'type': 'twisty.weight.RegularizedIntegral',
        },
    },
    'scatter': 1.,
    'absorption': 0.,
    'seed': '0123456789ABCDEF0123456789ABCDEF',
}


class RunnerTestCase(BaseTest):

    def setUp(self):
        self.config = copy.deepcopy(DEFAULT_CONFIG)

    def test_seed1(self):
        result = Runner.from_dict(self.config)
        result = Runner.from_dict(self.config)  # test for no modifications
        self.assertEqual('0123456789abcdef0123456789abcdef', result.seed)

    def test_seed2(self):
        del self.config['seed']
        result = Runner.from_dict(self.config)
        self.assertIsNotNone(result.seed)

    def test_curve1(self):
        runner = Runner.from_dict(self.config)
        curve = runner.curve
        self.assertIsNot(curve, runner.constraints.curve)
        self.assertIsNot(curve.basis, runner.constraints.curve.basis)
        self.assertIsNot(curve.origin, runner.constraints.curve.origin)
        self.assertIsNot(curve.segments, runner.constraints.curve.segments)
        self.assertFalse(any(x is y for x, y in
                             zip(curve.segments,
                                 runner.constraints.curve.segments)))

    def test_seed3(self):
        seed = 'f' * 32
        override = {
            'seed': seed,
        }
        result = Runner.from_dict(self.config, override=override)
        self.assertEqual(result.seed, seed)

    def test_from_dict1(self):
        override = {
            'bootstrap': {
                'segments': 200,
            }
        }
        result = Runner.from_dict(self.config, override=override)
        self.assertEqual(200, len(result.curve.segments))

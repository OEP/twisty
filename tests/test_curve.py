from twisty.curve import Curve

import numpy.testing as npt
import numpy as np

import unittest

from testlib import BaseTest


class StraightX(Curve):

    def position(self, s):
        return np.array([s, 0, 0])


class Polynomial(Curve):

    def position(self, s):
        return np.array([s ** 2, 0, s])


class Trigonometric(Curve):

    def position(self, s):
        return np.array([s, 3 * np.sin(s), 3 * np.cos(s)])

# TEST CASE: STRAIGHT X


class StraightXTestCase(BaseTest):

    def setUp(self):
        self.curve = StraightX()

    def test_position1(self):
        result = self.curve.position(0)
        expected = [0, 0, 0]
        npt.assert_almost_equal(result, expected)

    def test_position2(self):
        result = self.curve.position(1)
        expected = [1, 0, 0]
        npt.assert_almost_equal(result, expected)

    def test_grad1(self):
        result = self.curve.grad(0)
        expected = [1, 0, 0]
        npt.assert_almost_equal(result, expected)

    def test_grad2(self):
        result = self.curve.grad2(0)
        expected = [0, 0, 0]
        npt.assert_almost_equal(result, expected)

    def test_grad3(self):
        result = self.curve.grad3(0)
        expected = [0, 0, 0]
        npt.assert_almost_equal(result, expected, decimal=3)

    def test_curvature(self):
        result = self.curve.curvature(0)
        expected = 0
        npt.assert_almost_equal(result, expected)

    def test_speed(self):
        result = self.curve.speed(0)
        expected = 1
        npt.assert_almost_equal(result, expected)

    def test_arclength(self):
        result = self.curve.arclength(1.1)
        expected = 1.1
        npt.assert_almost_equal(result, expected)

### TEST CASE: POLYNOMIAL ###


class PolynomialTestCase(BaseTest):

    def setUp(self):
        self.curve = Polynomial()

    def test_position1(self):
        result = self.curve.position(0)
        expected = [0, 0, 0]
        npt.assert_almost_equal(result, expected)

    def test_position2(self):
        result = self.curve.position(2)
        expected = [4, 0, 2]
        npt.assert_almost_equal(result, expected)

    def test_speed1(self):
        result = self.curve.speed(2)
        expected = np.sqrt(17)
        npt.assert_almost_equal(result, expected)

    def test_arclength1(self):
        result = self.curve.arclength(2)
        expected = np.sqrt(17) + 0.25 * np.arcsinh(4)
        npt.assert_almost_equal(result, expected)

    def test_curvature1(self):
        result = self.curve.curvature(1)
        expected = 2. / (5. ** 1.5)
        npt.assert_almost_equal(result, expected, decimal=3)

    def test_torsion1(self):
        result = self.curve.torsion(1)
        expected = 0
        npt.assert_almost_equal(result, expected)

### TEST CASE: TRIGONOMETRIC ###


class TrigonometricTestCase(BaseTest):

    def setUp(self):
        self.curve = Trigonometric()

    def test_position1(self):
        result = self.curve.position(0)
        expected = [0, 0, 3]
        npt.assert_almost_equal(result, expected)

    def test_position2(self):
        result = self.curve.position(np.pi / 2)
        expected = [np.pi / 2, 3, 0]
        npt.assert_almost_equal(result, expected)

    def test_curvature1(self):
        result = self.curve.curvature(0)
        expected = 0.3
        npt.assert_almost_equal(result, expected, decimal=4)

    def test_curvature2(self):
        result = self.curve.curvature(.2)
        expected = 0.3
        npt.assert_almost_equal(result, expected, decimal=4)

    def test_torsion1(self):
        result = self.curve.torsion(0)
        expected = -9 / (9 ** 2 + 9.)
        npt.assert_almost_equal(result, expected, decimal=4)

    def test_torsion2(self):
        result = self.curve.torsion(np.pi / 2)
        expected = -9 / (9 ** 2 + 9.)
        npt.assert_almost_equal(result, expected, decimal=4)

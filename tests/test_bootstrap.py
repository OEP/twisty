from testlib import BaseTest

from twisty import bootstrap, geometry, parametric

import random

import numpy.testing as npt
import numpy as np


class ConstantHelicityTest(BaseTest):

    def setUp(self):
        from twisty.bootstrap import ConstantHelicity
        self.strategy = ConstantHelicity(
            step=1, segments=10, curvature=1, torsion=1
        )

    def test_create_curve(self):
        constraints = self.strategy.create_curve(random)
        curve = constraints.curve
        self.assertIsInstance(constraints, bootstrap.Constraints)
        self.assertIsNone(self.strategy.origin)
        self.assertIsNone(self.strategy.basis)
        self.assertEqual(1, curve.step)
        self.assertEqual(10, len(curve.segments))
        self.assertTrue(all(x.curvature == 1 and x.torsion == 1
                            for x in curve.segments))
        npt.assert_almost_equal(np.zeros(3), curve.origin)
        npt.assert_almost_equal(np.eye(3), curve.basis)


class RandomTest(BaseTest):

    def setUp(self):
        from twisty.bootstrap import Random
        self.strategy = Random(
            step=1, segments=10, curvature_max=1, torsion_max=1,
        )

    def test_create_curve(self):
        constraints = self.strategy.create_curve(random)
        curve = constraints.curve
        self.assertIsNone(self.strategy.origin)
        self.assertIsNone(self.strategy.basis)
        self.assertEqual(1, curve.step)
        self.assertEqual(10, len(curve.segments))
        npt.assert_almost_equal(np.zeros(3), curve.origin)
        npt.assert_almost_equal(np.eye(3), curve.basis)


class SimpleGeometryTestCase(BaseTest):

    def setUp(self):
        self.emitter = geometry.Ray((0, 0, 0), (1, 0, 0))
        self.sensor = geometry.Ray((1, 0, 0), (0, 1, 0))
        self.instance = bootstrap.SimpleGeometry(self.emitter, self.sensor, 10,
                                                 [0, 2])

    def test_from_dict1(self):
        d = {
            'emitter': self.emitter.to_dict(),
            'sensor': self.sensor.to_dict(),
            'resolution': 10,
            'arclength': [0, 2],
        }
        result = bootstrap.SimpleGeometry.from_dict(d)
        self.assertIsInstance(result.emitter, geometry.Ray)
        self.assertIsInstance(result.sensor, geometry.Ray)
        self.assertEqual(10, result.resolution)

    def test_to_dict1(self):
        result = self.instance.to_dict()
        expected = {
            'emitter': {
                'type': 'twisty.geometry.Ray',
                'origin': [0, 0, 0],
                'direction': [1, 0, 0],
            },
            'sensor': {
                'type': 'twisty.geometry.Ray',
                'origin': [1, 0, 0],
                'direction': [0, 1, 0],
            },
            'resolution': 10,
            'arclength': [0, 2],
        }
        self.assertEqual(expected, result)

    def test_create_curve1(self):
        result = self.instance.create_curve(random)
        self.assertIsInstance(result, bootstrap.Constraints)
        npt.assert_almost_equal([0, 0, 0], result.curve.origin)
        npt.assert_almost_equal([1, 0, 0], result.curve.basis[0])
        npt.assert_almost_equal([1, 0, 0], result.x1)
        npt.assert_almost_equal([0, 1, 0], result.n1)
        self.assertEqual(None, result.camera)


class CreateCurveTestCase(BaseTest):

    def setUp(self):
        self.spec = bootstrap.CurveSpec(x0=[0, 0, 0], n0=[1, 0, 0],
                                        x1=[1, 0, 0], n1=[1, 0, 0],
                                        arclength=[0, 2])
        self.rng = random.Random(0)

    def _create_curve(self, spec=None, **kw):
        if spec is None:
            spec = self.spec
        kw.setdefault('rng', self.rng)
        kw.setdefault('l0', .1)
        kw.setdefault('l1', .1)
        kw.setdefault('n2', [0, 1, 0])
        kw.setdefault('arclength_goal', 1.5)
        return bootstrap.create_curve(spec, **kw)

    def test_create_curve1(self):
        result = self._create_curve()
        self.assertIsInstance(result, parametric.Bezier)
        npt.assert_almost_equal(result.position(0), self.spec.x0)
        npt.assert_almost_equal(result.position(1), self.spec.x1)
        npt.assert_almost_equal(result.tangent(0), self.spec.n0)
        npt.assert_almost_equal(result.tangent(1), self.spec.n1)
        self.assertTrue(0 < result.arclength(1) < 2)

    def test_create_curve2(self):
        '''Restrictive curve spec raises CreateFailed'''
        self.spec.arclength = [1.5, 2]
        with self.assertRaises(bootstrap.CreateFailed):
            self._create_curve(project_max=1)

from testlib import BaseTest
from twisty.gfx import rotation_matrix, rotate, Camera
import numpy as np
import numpy.testing as npt
import math

x = np.array([1, 0, 0])
y = np.array([0, 1, 0])
z = np.array([0, 0, 1])


class RotationTest(BaseTest):

    def test_rotation1(self):
        npt.assert_almost_equal(x, rotate(x, x, 0))

    def test_rotation2(self):
        npt.assert_almost_equal(y, rotate(y, y, 0))

    def test_rotation3(self):
        npt.assert_almost_equal(z, rotate(z, z, 0))

    def test_rotation4(self):
        npt.assert_almost_equal(x, rotate(y, -z, math.pi / 2))

    def test_rotation5(self):
        npt.assert_almost_equal(x, rotate(z, y, math.pi / 2))

    def test_rotation6(self):
        npt.assert_almost_equal(y, rotate(x, z, math.pi / 2))

    def test_rotation7(self):
        npt.assert_almost_equal(y, rotate(z, -x, math.pi / 2))

    def test_rotation8(self):
        npt.assert_almost_equal(z, rotate(y, x, math.pi / 2))

    def test_rotation9(self):
        npt.assert_almost_equal(z, rotate(x, -y, math.pi / 2))


class CameraTest(BaseTest):

    def setUp(self):
        self.camera = Camera([0, 0, 0], [0, 0, 1])

    def test_eye1(self):
        self.assertIsInstance(self.camera.eye, np.ndarray)
        npt.assert_almost_equal([0, 0, 0], self.camera.eye)

    def test_eye2(self):
        self.camera.eye = (0, 1, 1)
        self.assertIsInstance(self.camera.eye, np.ndarray)
        npt.assert_almost_equal([0, 1, 1], self.camera.eye)

    def test_view1(self):
        self.assertIsInstance(self.camera.view, np.ndarray)
        npt.assert_almost_equal([0, 0, 1], self.camera.view)
        npt.assert_almost_equal(0, self.camera.up.dot(self.camera.view))

    def test_view2(self):
        self.camera.view = [2, 0, 0]
        self.assertIsInstance(self.camera.view, np.ndarray)
        npt.assert_almost_equal([1, 0, 0], self.camera.view)
        npt.assert_almost_equal(0, self.camera.up.dot(self.camera.view))

    def test_up1(self):
        self.assertIsInstance(self.camera.up, np.ndarray)
        npt.assert_almost_equal([0, 1, 0], self.camera.up)
        npt.assert_almost_equal(0, self.camera.up.dot(self.camera.view))

    def test_up2(self):
        self.camera.up = [2, 0, 0]
        self.assertIsInstance(self.camera.up, np.ndarray)
        npt.assert_almost_equal([1, 0, 0], self.camera.up)
        npt.assert_almost_equal(0, self.camera.up.dot(self.camera.view))

    def test_right1(self):
        self.assertIsInstance(self.camera.right, np.ndarray)
        npt.assert_almost_equal(0, self.camera.up.dot(self.camera.right))
        npt.assert_almost_equal(0, self.camera.view.dot(self.camera.right))

    def test_right2(self):
        self.assertRaises(
            AttributeError,
            setattr,
            self.camera,
            'right',
            (0,
             1,
             0))

    def test_cast1(self):
        result = self.camera.cast(0.5, 0.5)
        self.assertIsInstance(result, np.ndarray)
        npt.assert_almost_equal(result, self.camera.view)

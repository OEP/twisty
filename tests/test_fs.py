"""Tests loose functions in twisty.fs module.
"""
import numpy as np
import numpy.testing as npt
from testlib import BaseTest

from twisty.gfx import normalize

import unittest


class FsModuleTest(BaseTest):

    def test_estimate_tangents(self):
        from twisty.fs import estimate_tangents

        # at least two points are required
        x1 = np.array([1, 0, 0])
        self.assertRaises(ValueError, estimate_tangents, [])
        self.assertRaises(ValueError, estimate_tangents, [x1])

        # two points is a simple case with the same unit tangent at endpoints
        expected_item = np.array([1, 0, 0])
        expected = [expected_item] * 2
        x2 = np.array([2, 0, 0])
        result = estimate_tangents([x1, x2])
        npt.assert_almost_equal(expected, result)

        # the tangents between the internal points will be averaged
        points = [
            np.array([0, 0, 0]),
            np.array([1, 0, 0]),
            np.array([1, 1, 0]),
        ]
        x1, x2, x3 = points
        expected = [
            normalize(x2 - x1),
            normalize(0.5 * (normalize(x3 - x2) + normalize(x2 - x1))),
            normalize(x3 - x2),
        ]
        result = estimate_tangents(points)
        npt.assert_almost_equal(expected, result)

    def test_rmf(self):
        from twisty.fs import rmf
        basis = np.eye(3)
        points = [
            [0, 0, 0],
            [0, 0, 2],
            [0, 0, 4],
        ]

        # Simple test which makes sure the rmf algorithm preserves the original
        # basis when the points are perfectly lined up along the initial tangent
        # vector.
        bases = rmf(points, basis)
        self.assertIsInstance(bases, list)
        self.assertEqual(len(points), len(bases))
        self.assertTrue(all(isinstance(x, np.ndarray) for x in bases))
        self.assertTrue(all(x.shape == (3, 3) for x in bases))
        npt.assert_almost_equal([basis] * 3, bases)

    def test_frames_to_ct(self):
        from twisty.fs import frames_to_ct, FS

        # setup
        step = 0.01
        curve = FS(np.eye(3), step)
        torsions = curvatures = np.arange(0, 5, 0.1)
        curve.segments = (
            FS.Segment(k, T)
            for k, T in zip(curvatures, torsions)
        )

        # By using the frames of the Frenet-Serret curve, passing these in to
        # frames_to_ct() we should get back the same curvatures and torsions.
        bases = tuple(curve.bases())
        result = frames_to_ct(bases, step)
        expected = zip(curvatures, torsions)
        npt.assert_allclose(result, expected, rtol=1e-3)


class FromPointsTestCase(BaseTest):

    def setUp(self):
        from twisty.fs import FS
        self.basis = np.eye(3)
        self.step = 0.01
        self.x0 = np.array([0, 0, 0])

        tangents = [np.array([1, 0, 0]) for i in range(200)]
        self.points = [
            self.x0 +
            self.step *
            t *
            i for i,
            t in enumerate(tangents)]
        self.result = FS.from_points(self.basis, self.step, self.points)

    def test_basis(self):
        npt.assert_almost_equal(self.result.basis, self.basis)

    def test_step(self):
        npt.assert_almost_equal(self.result.step, self.step)

    def test_points(self):
        npt.assert_almost_equal(self.points, tuple(self.result.points()))


class FromPointsTestCase2(FromPointsTestCase):

    def setUp(self):
        from twisty.fs import FS
        self.basis = np.eye(3)
        self.step = 0.01
        self.x0 = np.array([0, 0, 0])

        theta = np.linspace(0, 2 * np.pi, 200)
        tangents = [np.array([np.sin(t), np.cos(t), 0]) for t in theta]
        self.points = [self.x0] + [None] * len(tangents)
        for i, t in enumerate(tangents):
            self.points[i + 1] = self.points[i] + self.step * t
        self.result = FS.from_points(self.basis, self.step, self.points)

    @unittest.expectedFailure
    def test_points(self):
        super(FromPointsTestCase2, self).test_points()

import os
import shutil
import tempfile
from unittest import TestCase
from cStringIO import StringIO

from twisty.storage import CSV, Record
from twisty import storage


class CSVTestCase(TestCase):

    def setUp(self):
        self.buffer = StringIO()
        self.instance = CSV(self.buffer)

    def test_store(self):
        record = Record(foo='foo', bar='bar')
        self.instance.store(None, record)
        self.assertEqual('# bar,foo\nbar,foo\n', self.buffer.getvalue())

    def test_read(self):
        '''Read is not possible'''
        with self.assertRaises(NotImplementedError):
            self.instance.read('f' * 32)

    def test_open(self):
        '''Opens and closes files'''
        CSV(os.devnull)


class LocalTestCase(TestCase):

    def setUp(self):
        self.root = tempfile.mkdtemp()
        self.instance = storage.Local(self.root)
        self.key = 'f' * 32
        self.record = Record(foo='foo', bar='bar')

    def tearDown(self):
        shutil.rmtree(self.root)

    def test_read1(self):
        '''Can't read nonexistant records'''
        with self.assertRaises(KeyError):
            self.instance.read(self.key)

    def test_read2(self):
        '''Reads data back correctly'''
        self.instance.store(self.key, self.record)
        result = self.instance.read(self.key)
        self.assertEqual(self.record._asdict(), result._asdict())

    def test_store(self):
        '''Storing things twice is OK'''
        self.instance.store(self.key, self.record)
        self.instance.store(self.key, self.record)


class CompositeTestCase(TestCase):

    def setUp(self):
        self.root1 = tempfile.mkdtemp()
        self.root2 = tempfile.mkdtemp()
        self.local1 = storage.Local(self.root1)
        self.local2 = storage.Local(self.root2)
        self.key = 'f' * 32
        self.record = Record(foo='foo', bar='bar')
        self.composite = storage.Composite({
            '001_local1': self.local1,
            '002_local2': self.local2,
        })

    def tearDown(self):
        shutil.rmtree(self.root1)
        shutil.rmtree(self.root2)

    def test_read(self):
        '''Can't read nonexistant records'''
        with self.assertRaises(KeyError):
            self.composite.read(self.key)

    def test_store1(self):
        '''First local backend takes keys preferentially'''
        self.composite.store(self.key, self.record)
        r1 = self.composite.read(self.key)
        r2 = self.local1.read(self.key)
        self.assertEqual(self.record._asdict(), r1._asdict())
        self.assertEqual(self.record._asdict(), r2._asdict())

    def test_store2(self):
        '''Second backend takes over if first fails'''
        self.local1.root = None
        try:
            self.composite.store(self.key, self.record)
            r1 = self.composite.read(self.key)
            r2 = self.local2.read(self.key)
            self.assertEqual(self.record._asdict(), r1._asdict())
            self.assertEqual(self.record._asdict(), r2._asdict())
        finally:
            self.local1.root = self.root1

    def test_store3(self):
        '''If all fail an exception is raised'''
        self.local1.root = None
        self.local2.root = None
        try:
            with self.assertRaises(storage.StorageError):
                self.composite.store(self.key, self.record)
        finally:
            self.local1.root = self.root1
            self.local2.root = self.root2

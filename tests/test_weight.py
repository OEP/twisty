from twisty.weight import RegularizedIntegral, CachedIntegral, weight_path
from twisty.fs import FS
from testlib import BaseTest
import numpy as np


class WeightTest(BaseTest):

    def setUp(self):
        self.curve = FS(np.eye(3, 3), 0.1)
        self.curve.segments = [FS.Segment(1, 1) for i in range(50)]

    def test_compute_weight(self):
        numerical = RegularizedIntegral(eps=0.5)
        cached = CachedIntegral(numerical)
        weight_path(self.curve, 1, 1, numerical)
        weight_path(self.curve, 1, 1, cached)

    def test_to_dict_regularized(self):
        strategy = RegularizedIntegral(eps=0.5, mu=1, step=1, bounds=None)
        result = strategy.to_dict()
        expected = {
            'type': 'twisty.weight.RegularizedIntegral',
            'eps': 0.5,
            'mu': 1,
            'step': 1,
            'bounds': None,
        }
        self.assertEqual(expected, result)

if __name__ == "__main__":
    import unittest
    unittest.main()

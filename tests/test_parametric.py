from twisty.parametric import Parametric, sine_step, Bezier
from testlib import BaseTest

import numpy.testing as npt
import numpy as np
import scipy

import unittest

norm = np.linalg.norm


def path_steps(x):
    differences = x[1:] - x[:-1]
    differences_squared_sum = (differences ** 2).sum(axis=1)
    return scipy.sqrt(differences_squared_sum)


def path_length(x):
    return path_steps(x).sum()


class BoundaryTestCase(object):

    n0 = np.array([1, 0, 0])
    n1 = np.array([1, 0, 0])
    x0 = np.zeros(3)
    x1 = np.ones(3)
    length = 3

    def test_x0_scalar(self):
        npt.assert_almost_equal(self.x0, self.instance.position(0))

    def test_x0_vector(self):
        npt.assert_almost_equal(
            [self.x0, self.x0], self.instance.position([0, 0]))

    def test_x1_scalar(self):
        l = self.length
        npt.assert_almost_equal(self.x1, self.instance.position(l))

    def test_x1_vector(self):
        l = self.length
        npt.assert_almost_equal(
            [self.x1, self.x1], self.instance.position([l, l]))

    def test_n0(self):
        npt.assert_almost_equal(self.n0, self.instance.tangent(0))

    def test_n1(self):
        npt.assert_almost_equal(self.n1, self.instance.tangent(self.length))


class BaseStepTestCase(object):

    def test_step1(self):
        self.assertEqual(0, self.step(0))

    def test_step2(self):
        self.assertEqual(1, self.step(1))

    def test_step3(self):
        e = 1e-12
        npt.assert_almost_equal(0, abs((self.step(1) - self.step(1 - e)) / e))

### PARAMETRIC FORMULATION TEST CASES ###


class ParametricTestCase(BoundaryTestCase):

    freq = 0

    def setUp(self):
        self.instance = Parametric(length=self.length, x0=self.x0, x1=self.x1,
                                   n0=self.n0, n1=self.n1, freq=self.freq)


class ParametricTestCase1(ParametricTestCase, BaseTest):
    pass


class ParametricTestCase2(ParametricTestCase, BaseTest):

    def setUp(self):
        self.length = 5
        super(ParametricTestCase2, self).setUp()


class ParametricTestCase3(ParametricTestCase, BaseTest):

    def setUp(self):
        self.n1 = -self.n1
        super(ParametricTestCase3, self).setUp()


class ParametricTestCase4(ParametricTestCase, BaseTest):

    def setUp(self):
        self.x1 = -np.ones(3)
        super(ParametricTestCase4, self).setUp()


class ParametricTestCase5(ParametricTestCase, BaseTest):

    n1 = np.array([0, 0, 1])

    def test_curvature0(self):
        result = self.instance.curvature(0)
        self.assertIsInstance(result, np.ndarray)
        self.assertEqual((), result.shape)

    def test_curvature1(self):
        s1, s2, s3 = 0, 0.5, 1.0
        c1, c2, c3 = (
            self.instance.curvature(s1),
            self.instance.curvature(s2),
            self.instance.curvature(s3),
        )
        expected = np.array([c1, c2, c3])
        result = self.instance.curvature([s1, s2, s3])

        # Curvatures should not all be 0.
        self.assertTrue((abs(expected) > 1e-6).any())

        # Result is always an ndarray
        self.assertIsInstance(result, np.ndarray)

        # Result matches shape of input
        self.assertEqual((3,), result.shape)

        # Array vs. scalar invocation is equivalent
        npt.assert_almost_equal(expected, result)

    def test_torsion0(self):
        result = self.instance.torsion(0)
        self.assertIsInstance(result, np.ndarray)
        self.assertEqual((), result.shape)

    @unittest.expectedFailure
    def test_torsion1(self):
        s1, s2, s3, s4, s5 = 0, 0.5, 1.0, 1.5, 2.0
        c1, c2, c3, c4, c5 = (
            self.instance.torsion(s1),
            self.instance.torsion(s2),
            self.instance.torsion(s3),
            self.instance.torsion(s4),
            self.instance.torsion(s5),
        )
        expected = np.array([c1, c2, c3, c4, c5])
        result = self.instance.torsion([s1, s2, s3, s4, s5], force=True)

        # Torsions should not all be 0.
        self.assertTrue((abs(expected) > 1e-6).any())

        # Result is always an ndarray
        self.assertIsInstance(result, np.ndarray)

        # Result matches shape of input
        self.assertEqual((5,), result.shape)

        # Array vs. scalar invocation is equivalent
        npt.assert_almost_equal(expected, result)

    def test_torsion2(self):
        # Make sure force=False works.
        self.instance.torsion(0)
        self.instance.torsion(np.array(0))
        self.assertRaises(ValueError, self.instance.torsion, [0, 1, 2])

    def test_speed1(self):
        npt.assert_almost_equal(1, self.instance.speed(0))

    def test_speed2(self):
        npt.assert_almost_equal(1, self.instance.speed(self.length))

    def test_speed3(self):
        s = [0, self.length]
        npt.assert_almost_equal([1, 1], self.instance.speed(s))

    def test_arclength1(self):
        # Arclength at position 0 should always be 0 by definition
        self.assertEqual(0, self.instance.arclength(0))

    def test_arclength2(self):
        x = self.instance.position(np.linspace(0, self.length, 20000))
        l = path_length(x)
        npt.assert_almost_equal(
            l, self.instance.arclength(
                self.instance.length))

    def test_argmap1(self):
        result = self.instance.argmap(0)
        expected = 0
        npt.assert_almost_equal(expected, result, decimal=6)
        npt.assert_almost_equal(result, self.instance.arclength(0), decimal=6)

    def test_argmap2(self):
        # Test tolerance parameter
        result = self.instance.argmap(0, xtol=1e-7)
        expected = 0
        npt.assert_almost_equal(expected, result, decimal=7)
        npt.assert_almost_equal(result, self.instance.arclength(0), decimal=7)

    def test_argmap3(self):
        # Test arbitrary point
        s = 1.3
        l = self.instance.arclength(s)
        result = self.instance.argmap(l, xtol=1e-7)
        expected = s
        npt.assert_almost_equal(expected, result, decimal=7)

    def test_argsample1(self):
        result = self.instance.argsample(2, xtol=1e-7)
        expected = [0, self.length]
        npt.assert_almost_equal(expected, result)

    @unittest.skip('known inaccuracy problem')
    def test_argsample2(self):
        result = self.instance.argsample(10)
        arclength = self.instance.arclength(self.length)
        step = arclength / 9

        arclengths = np.array([self.instance.arclength(x) for x in result])
        intervals = arclengths[1:] - arclengths[:-1]

        npt.assert_almost_equal(intervals, [step] * 9)


class BezierTestCase(BoundaryTestCase, BaseTest):

    def setUp(self):
        self.instance = Bezier(length=self.length)
        self.instance.control = [
            self.x0,
            self.x0 + self.n0,
            self.x1 - self.n1,
            self.x1
        ]


class BezierTestCase2(BoundaryTestCase, BaseTest):

    def setUp(self):
        self.instance = Bezier(length=self.length)
        self.instance.control = [
            self.x0,
            self.x0 + self.n0,
            (self.x0 + self.x1) / 2,
            self.x1 - self.n1,
            self.x1
        ]



### STEP FUNCTION TEST CASES ###

class StepTestCase1(BaseTest, BaseStepTestCase):

    def setUp(self):
        self.step = sine_step

### DISCRETE FRENET-SERRET CONVERSION ###


@unittest.skip('slow')
class DiscreteFSTestCase1(BaseTest):

    x0 = np.zeros(3)
    x1 = np.ones(3)
    n0 = np.array([1, 0, 0])
    n1 = np.array([0, 0, 1])
    length = 3.

    @classmethod
    def setUpClass(cls):
        cls.parametric = Parametric(x0=cls.x0, x1=cls.x1,
                                    n0=cls.n0, n1=cls.n1,
                                    length=cls.length)
        cls.fs, cls.arg = cls.parametric.to_fs(100, parameter=True)

    def test_x1(self):
        result = self.fs.end_point()
        npt.assert_almost_equal(self.x1, result)

    def test_n1(self):
        result = self.fs.end_basis()[0]
        npt.assert_almost_equal(self.n1, result)

    def test_segments(self):
        self.assertEqual(100, len(self.fs.segments))

    def test_step(self):
        expected = self.parametric.arclength(self.length) / 99
        self.assertEqual(expected, self.fs.step)

    def test_basis_orthogonal(self):
        '''Basis is right-hand orthogonal'''
        e1, e2, e3 = self.fs.basis
        npt.assert_almost_equal(e3, np.cross(e1, e2))
        npt.assert_almost_equal(e1, np.cross(e2, e3))
        npt.assert_almost_equal(e2, np.cross(e3, e1))

    def test_basis_normal(self):
        '''Basis vectors should be normal'''
        result = np.linalg.norm(self.fs.basis, axis=1)
        npt.assert_almost_equal([1, 1, 1], result)

    def test_basis_n0(self):
        '''First basis vector should be n0'''
        npt.assert_almost_equal(self.n0, self.fs.basis[0])

    def test_origin(self):
        npt.assert_almost_equal(self.x0, self.fs.origin)

    def test_curvature(self):
        result = [s.curvature for s in self.fs.segments]
        expected = map(self.parametric.curvature, self.arg)
        npt.assert_almost_equal(expected, result)

    def test_torsion(self):
        result = [s.torsion for s in self.fs.segments]
        expected = map(self.parametric.torsion, self.arg)
        npt.assert_almost_equal(expected, result)

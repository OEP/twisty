from twisty.fs import FS
from testlib import BaseTest
import numpy as np
import copy


class CopyTest(BaseTest):

    def setUp(self):
        self.curve = FS(np.eye(3, 3), 0.1)
        self.curve.segments = [FS.Segment(1, 1) for i in range(10)]

    def test_deepcopy(self):
        result = copy.deepcopy(self.curve)
        self.assertIsNot(self.curve, result)
        self.assertIsNot(self.curve.segments, result.segments)
        self.assertEqual(self.curve.segments, result.segments)
        for i, j in zip(self.curve.segments, result.segments):
            self.assertIsNot(i, j)

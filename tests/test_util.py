import unittest
import os


from twisty import util


class MergeDictTestCase(unittest.TestCase):

    def test_merge_dict1(self):
        '''Flat dictionaries are merged'''
        result = util.merge_dict({'a': 1}, {'b': 2})
        expected = {'a': 1, 'b': 2}
        self.assertEqual(result, expected)

    def test_merge_dict2(self):
        '''The second argument's values serve as overrides'''
        result = util.merge_dict({'a': 1}, {'a': 2})
        expected = {'a': 2}
        self.assertEqual(result, expected)

    def test_merge_dict3(self):
        '''The inner dictionaries are merged in the same way'''
        result = util.merge_dict({'a': {'b': 2}}, {'a': {'c': 3}})
        expected = {'a': {'b': 2, 'c': 3}}
        self.assertEqual(result, expected)

    def test_merge_dict4(self):
        '''The inner dictionaries are overridden in the same way'''
        result = util.merge_dict({'a': {'b': 2}}, {'a': {'b': 3}})
        expected = {'a': {'b': 3}}
        self.assertEqual(result, expected)


class GetSubclassTestCase(unittest.TestCase):

    class Base(object):
        pass

    class Subclass(Base):
        pass

    name = '.'.join([Subclass.__module__, Subclass.__name__])

    def test_get_subclass1(self):
        '''Properly finds subclasses'''
        result = util.get_subclass(self.Base, self.name)
        self.assertEqual(self.Subclass, result)

    def test_get_subclass2(self):
        '''Raises ValueError if it can't find a subclass'''
        with self.assertRaises(ValueError):
            util.get_subclass(self.Base, 'xxx')


class ImportNameTestCase(unittest.TestCase):

    def test_import_name1(self):
        '''Properly imports dotted-path names'''
        result = util.import_name('os.path.join')
        self.assertEqual(os.path.join, result)

    def test_import_name2(self):
        '''Raises AttributeError for invalid dotted-paths'''
        with self.assertRaises(AttributeError):
            util.import_name('os.path.xxx')


class PrettyPartialTestCase(unittest.TestCase):

    def _function(self, x, y=0):
        return x + y

    def test_str1(self):
        '''Shows keyword arguments'''
        partial = util.PrettyPartial(self._function, y=4)
        result = str(partial)
        self.assertEqual('_function(y=4)', result)

    def test_str2(self):
        '''Shows positional arguments'''
        partial = util.PrettyPartial(self._function, 2)
        result = str(partial)
        self.assertEqual('_function(2)', result)

    def test_str3(self):
        '''Shows positional and keyword arguments'''
        partial = util.PrettyPartial(self._function, 2, y=4)
        result = str(partial)
        self.assertEqual('_function(2, y=4)', result)

    def test_str4(self):
        '''Uses repr() to format'''
        partial = util.PrettyPartial(self._function, '2', y='4')
        result = str(partial)
        self.assertEqual('''_function('2', y='4')''', result)

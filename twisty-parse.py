from twisty.runner import Runner
import argparse
import sys
import yaml


def main():
    parser = _get_parser()
    args = parser.parse_args()

    if args.output is None:
        args.output = sys.stdout
    else:
        args.output = open(args.output, 'w')

    # Triggers an error if something is wrong with the configuration file.
    Runner.from_config(args.config)

    config = Runner.parse_config(args.config)
    yaml.safe_dump(config, stream=args.output)


def _get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('config', help='YAML configuration file')
    parser.add_argument('-o', '--output',
                        help='Where to output the result; stdout by default')
    return parser

if __name__ == '__main__':
    main()

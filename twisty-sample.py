#!/usr/bin/env python

from twisty.runner import Runner, PerturbFailed
from twisty.storage import Record
from argparse import ArgumentParser
import time
import numpy as np


def main():
    parser = _get_parser()
    args = parser.parse_args()

    # The rest of this script we should consider something which we are
    # interested in the performance of.
    t0 = time.time()

    runner = Runner.from_config(args.config)

    # Convenience names.
    step = runner.curve.step
    segments = len(runner.curve.segments)
    constraints = runner.constraints
    camera = constraints.camera
    n1 = runner.constraints.n1
    x1 = runner.constraints.x1
    kw = {}
    status = 'ok'

    # Some extra data we can get when there is a camera for the experiment.
    if camera is not None:
        kw = {
            'theta': np.arccos(np.dot(n1, camera.view)),
            'view_x': camera.view[0],
            'view_y': camera.view[1],
            'view_z': camera.view[2],
            'up_x': camera.up[0],
            'up_y': camera.up[1],
            'up_z': camera.up[2],
            'right_x': camera.right[0],
            'right_y': camera.right[1],
            'right_z': camera.right[2],
        }

    sum_ = 0.
    count = 0

    # Perturb curve for a fixed number of iterations.
    for i in xrange(args.skip + args.count):
        try:
            runner.next(args.max_tries)
        except PerturbFailed:
            status = 'aborted'
            count = 0
            sum_ = 0
            break

        # We may skip the first few curves (i.e. to avoid startup bias).
        if i < args.skip:
            continue

        # Accumulate otherwise.
        sum_ += runner.path_weight
        count += 1

    # Log how long it took.
    dt = time.time() - t0

    # Store the result.
    record = Record(time=dt, run_id=runner.id, seed=runner.seed, step=step,
                    x1_x=x1[0], x1_y=x1[1], x1_z=x1[2],
                    n1_x=n1[0], n1_y=n1[1], n1_z=n1[2], segments=segments,
                    status=status,
                    sum=sum_, count=count, **kw)
    runner.storage.store(record.seed, record)


def _get_parser():
    parser = ArgumentParser()
    parser.add_argument('config', help='Config file to use')
    parser.add_argument('skip', type=int,
                        help='Number of initial samples to skip')
    parser.add_argument('count', type=int,
                        help='Number of samples to take after skipping')
    parser.add_argument('-m', '--max-tries', type=int, default=None,
                        help='Exit normally if N perturbation attempts fail')
    return parser


if __name__ == "__main__":
    main()

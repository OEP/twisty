#!/usr/bin/env python
'''
Plot intensity in a heat-map like fashion.
'''

from plotlib import savefig, read_csv

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import numpy as np

import argparse
import sys

DESCRIPTION = '''\
Plot a calculated beam spread function (BSF) in a 2D projection.

Any CSV file can be plotted technically, but this script expects the following:

* The columns ``x`` and ``y`` which are the x- and y-coordinate of the bin.
* A column ``weight_mean`` for the mean of each bin.
* A column ``count_total`` for total samples in each bin.
* The 2D grid is square.
'''


def main():
    parser = _get_parser()
    args = parser.parse_args()

    plt.figure(figsize=args.figsize)

    # Decide which scale to use
    norm = None
    if args.scale == 'log':
        norm = LogNorm()
    elif args.scale is not None:
        assert False, 'Invalid scale: %s' % args.scale

    # Decide a colormap to use
    cmap = cm.get_cmap(args.cmap)

    # Default to the 0-value of the colormap if not specified.
    if args.bgcolor is None:
        args.bgcolor = cmap(0)

    # Read and extract the data we want to plot.
    df = read_csv(args.data)
    x, y, weight = df[['x', 'y', 'weight_mean']].values.transpose()

    if args.no_weight:
        weight = df['count_total'].values

    # Assume the shape of the image is square.
    bins = int(np.sqrt(x.shape[0]))

    # Normalize with nan support
    if args.normed:
        weight /= weight[~np.isnan(weight)].sum()

    plt.hist2d(x, y, weights=weight, norm=norm, bins=bins)
    bar = plt.colorbar()
    if args.no_weight:
        bar.set_label('Frequency')
    else:
        bar.set_label('Relative intensity')
    plt.xlabel('x')
    plt.ylabel('y')
    ax = plt.gca()
    ax.set_axis_bgcolor(args.bgcolor)

    savefig(sys.stdout, format=args.format)


def _get_parser():
    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('data', help='CSV data to plot')
    parser.add_argument('-n', '--normed', default=False, action='store_true',
                        help='Normalize such that weights sum to 1')
    parser.add_argument('-c', '--cmap', default='gray',
                        help='Use alternate color map')
    parser.add_argument('-W', '--no-weight', default=False,
                        action='store_true',
                        help='Plot sample count instead of weight')
    parser.add_argument('-s', '--scale', default=None,
                        help='Use log or linear scale')
    parser.add_argument('-B', '--bgcolor',
                        help='Change background color; see matplotlib colors')
    parser.add_argument('-f', '--format', default='png',
                        help='Image output format')
    parser.add_argument('--figsize', nargs=2, type=float,
                        help='Figure size (inches)')
    return parser


if __name__ == '__main__':
    main()

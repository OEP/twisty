#!/usr/bin/env python

from plotlib import savefig, read_csv, fit_gaussian

import matplotlib.pyplot as plt

import argparse
import sys

DESCRIPTION = '''\
Residuals plot for Gaussian fit of beam spread function data.
'''


def main():
    parser = _get_parser()
    args = parser.parse_args()

    plt.figure(figsize=args.figsize)

    df = read_csv(args.data)
    angle = df['angle']
    averages = df['average_weight']

    if args.normalize_to is not None:
        averages *= args.normalize_to / averages.max()

    _, _, fit = fit_gaussian(angle, averages, force_peak=averages.max())

    predictions = fit(angle)
    plt.scatter(predictions, predictions - averages)

    # Draw y=0 line.
    plt.axhline(linestyle='dashed', color='black')

    ylim = max(map(abs, plt.ylim()))
    plt.ylim(-ylim, ylim)

    plt.xlabel('Predictions')
    plt.ylabel('Residuals')

    if args.tight:
        plt.tight_layout()
    if args.xlim:
        plt.xlim(args.xlim)

    savefig(sys.stdout)


def _get_parser():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('data', help='CSV data to plot')

    parser.add_argument('-n', '--normalize-to', type=float,
                        help='Normalize peak of BSF to this value')
    parser.add_argument('-f', '--figsize', nargs=2, type=float,
                        help='Figure size (inches)')
    parser.add_argument('-t', '--tight', action='store_true',
                        help='Use matplotlib tight_layout()')
    parser.add_argument('-x', '--xlim', nargs=2, type=float,
                        help='Set limits of x-axis')
    return parser

if __name__ == '__main__':
    main()

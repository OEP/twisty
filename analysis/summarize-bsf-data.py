#!/usr/bin/env python
'''
Print out summary of BSF data.
'''

import argparse
import sys
import pandas as pd
from collections import defaultdict
from plotlib import read_csv, label_from_filename, fit_gaussian
from math import pi


def main():
    '''Main entry point'''
    parser = _get_parser()
    args = parser.parse_args()

    df = defaultdict(list)
    for path in args.data:
        label = label_from_filename(path)
        data = read_csv(path)

        name = df['Name']
        walltime = df['Wall time']
        count = df['Sample count']
        efficiency = df['Efficiency']
        gaussian_width = df['Gaussian width']

        normalized_weight = (data['average_weight'] *
                             0.025 / data['average_weight'].max())
        angle_deg = data['angle'] * 180. / pi
        param, _, _ = fit_gaussian(angle_deg, normalized_weight,
                                   force_peak=normalized_weight.max())
        width, = param

        aborted_total = data['aborted_total'].sum()
        ok_total = data['ok_total'].sum()
        sample_count = ok_total + aborted_total
        eff = 100 * ok_total / float(sample_count)

        name.append(label)
        years = data['time_total'].sum() / 3.154e+7
        walltime.append('%.2f years' % years)
        count.append('%.2e' % sample_count)
        efficiency.append('%.2f%%' % eff)
        gaussian_width.append('%.2f' % width)

    index = df.pop('Name')
    df = pd.DataFrame(df, index=index)
    df.to_latex(sys.stdout)


def _get_parser():
    '''Construct argument parser'''
    parser = argparse.ArgumentParser()
    parser.add_argument('data', nargs='+')
    return parser

if __name__ == '__main__':
    main()

#!/usr/bin/env python
'''
Plot intensity vs. angle.

'''

from plotlib import read_csv

import numpy as np
import pandas as pd

import argparse
import sys

CHUNKSIZE = 10000
DESCRIPTION = '''\
Create a 2D binning of beam spread function data. This assigns an xy-coordinate
based on the dot product of a "camera" right and up vector with the `n1' end
tangent vector chosen for the sample.
'''


def main():
    parser = _get_parser()
    args = parser.parse_args()

    result = _get_bin_result(args.data, args)
    df = pd.DataFrame(result)
    df.to_csv(sys.stdout, index=False)


def _notice(message, fp=None, newline=True, flush=False, ttyonly=True):
    if fp is None:
        fp = sys.stderr
    if ttyonly and not fp.isatty():
        return
    fp.write(message)
    if newline:
        fp.write('\n')
    if flush:
        fp.flush()


def _get_bin_result(path, args, label=None):
    # Construct the globals over each data set.
    edges_x = np.linspace(-1, 1, args.bins + 1)
    edges_y = np.linspace(-1, 1, args.bins + 1)

    # Running standard deviation
    weight_statistic = Binned2DStatistic(edges_x, edges_y)
    count_statistic = Binned2DStatistic(edges_x, edges_y)
    time_statistic = Binned2DStatistic(edges_x, edges_y)
    step_statistic = Binned2DStatistic(edges_x, edges_y)
    ok_statistic = Binned2DStatistic(edges_x, edges_y)
    aborted_statistic = Binned2DStatistic(edges_x, edges_y)
    ok_step_statistic = Binned2DStatistic(edges_x, edges_y)
    aborted_step_statistic = Binned2DStatistic(edges_x, edges_y)
    ok_time_statistic = Binned2DStatistic(edges_x, edges_y)
    aborted_time_statistic = Binned2DStatistic(edges_x, edges_y)

    _notice('Binning {}'.format(path), newline=False)

    for df in read_csv(path, chunksize=CHUNKSIZE, error_bad_lines=False):
        n1 = df[['n1_x', 'n1_y', 'n1_z']].values
        try:
            up = df[['up_x', 'up_y', 'up_z']].values
            right = df[['right_x', 'right_y', 'right_z']].values
        except KeyError:
            up = np.array(args.up)
            right = np.array(args.right)

        # Compute x values
        obs_x = n1.dot(right)
        obs_y = n1.dot(up)

        # Update the running statistics
        weight_statistic.add_observations(obs_x, obs_y, df['sum'])
        count_statistic.add_observations(obs_x, obs_y, df['count'])
        time_statistic.add_observations(obs_x, obs_y, df['time'])
        step_statistic.add_observations(obs_x, obs_y, df['step'])

        # Old data sets may not have 'status' field so this is for backwards
        # compatibility. Previously, only 'ok' samples were reported which is
        # the reason for the assumption that all samples are 'ok'.
        try:
            df['status']
        except KeyError:
            df['status'] = np.repeat('ok', len(df))

        ok = (df['status'] == 'ok').values
        aborted = (df['status'] == 'aborted').values
        ok_statistic.add_observations(obs_x, obs_y, ok)
        aborted_statistic.add_observations(obs_x, obs_y, aborted)

        # These few statistics look at the behavior of a variable given the
        # status variable. For instance, it is nice to know what amount of our
        # time we are putting toward samples that are aborted, or what path
        # lengths tend to cause an aborted sample.
        if ok.any():
            ok_step_statistic.add_observations(obs_x[ok], obs_y[ok],
                                               df['step'][ok])
            ok_time_statistic.add_observations(obs_x[ok], obs_y[ok],
                                               df['time'][ok])
        if aborted.any():
            aborted_step_statistic.add_observations(
                obs_x[aborted], obs_y[aborted], df['step'][aborted])
            aborted_time_statistic.add_observations(
                obs_x[aborted], obs_y[aborted], df['time'][aborted])

        # Simple progress indicator
        _notice('.', newline=False, flush=True)
    _notice('\n')

    ox, oy = np.meshgrid((edges_x[:-1] + edges_x[1:]) / 2,
                         (edges_y[:-1] + edges_y[1:]) / 2)
    ox = ox.flatten()
    oy = oy.flatten()

    return {
        'weight_mean': weight_statistic.mean,
        'weight_total': weight_statistic.total,
        'count_total': count_statistic.total,
        'x': ox,
        'y': oy,
        'time_total': time_statistic.total,
        'time_mean': time_statistic.mean,
        'step_total': step_statistic.total,
        'step_mean': step_statistic.mean,
        'ok_total': ok_statistic.total,
        'ok_mean': ok_statistic.mean,
        'aborted_total': aborted_statistic.total,
        'aborted_mean': aborted_statistic.mean,

        # Time variable partitioned by status
        'ok_time_total': ok_time_statistic.total,
        'ok_time_mean': ok_time_statistic.mean,
        'aborted_time_total': aborted_time_statistic.total,
        'aborted_time_mean': aborted_time_statistic.mean,

        # Step variable partitioned by status
        'ok_step_total': ok_step_statistic.total,
        'ok_step_mean': ok_step_statistic.mean,
        'aborted_step_total': aborted_step_statistic.total,
        'aborted_step_mean': aborted_step_statistic.mean,
    }


class Binned2DStatistic(object):

    def __init__(self, edges_x, edges_y, dtype=np.float128):
        self.edges_x = np.asarray(edges_x)
        self.edges_y = np.asarray(edges_y)
        self.dtype = dtype
        size = [len(self.edges_x) - 1, len(self.edges_y) - 1]
        self._total = np.zeros(size, dtype=dtype)
        self._count = np.zeros(size, dtype=dtype)

    def __len__(self):
        return len(self.edges) - 1

    @property
    def mean(self):
        '''The sample mean'''
        return np.array(self.total / self.count)

    @property
    def total(self):
        return self._total.flatten()

    @property
    def count(self):
        return self._count.flatten()

    # @property
    # def sem(self):
    #     '''The sample standard error of the mean'''
    #     return self.stdev / (self.count)

    # @property
    # def variance(self):
    #     '''The sample variance'''
    #     return self.stdev ** 2

    def add_observations(self, x, y, z):
        assert len(x) == len(z), 'Observations must have same size as x-coord'
        assert len(y) == len(z), 'Observations must have same size as y-coord'

        bins = [self.edges_x, self.edges_y]
        values, _, _ = np.histogram2d(x, y, bins=bins, weights=z)
        counts, _, _ = np.histogram2d(x, y, bins=bins)
        self._total += values
        self._count += counts


def _get_parser():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('data', help='Data set to reduce')
    parser.add_argument('-b', '--bins', default=50, type=int,
                        help='Number of bins to reduce data to')
    parser.add_argument('-u', '--up', nargs=3, type=float,
                        default=(0, 1, 0), help='Camera view vector')
    parser.add_argument('-r', '--right', nargs=3, type=float,
                        default=(0, 0, -1), help='Camera right vector')
    return parser


if __name__ == '__main__':
    main()

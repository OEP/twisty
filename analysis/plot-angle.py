#!/usr/bin/env python
'''
Plot intensity vs. angle.

'''

from plotlib import (savefig, read_csv, label_from_filename, fit_gaussian,
                     fit_lorentzian, gaussian)

from matplotlib import rc
import numpy as np
import matplotlib.pyplot as plt

import argparse
import math
import sys

rc('legend', fontsize='small')

CHUNKSIZE = 10000
DESCRIPTION = '''\
Plot a calculated beam spread function (BSF).

Any CSV file can be plotted technically, but this script expects the following:

* A column ``average_weight`` which will be the y-value.
* A column ``angle`` which will be the x-value.
* A column ``count`` which is used for histograms.
* A column ``weight_sem`` which serves as the error bars.
* The ``angle`` column is in ascending order.
'''


def main():
    parser = _get_parser()
    args = parser.parse_args()

    # Assume the plot bounds go from 0 to pi for now...
    if args.xlim is None:
        args.xlim = np.array((0, 1), dtype=np.float)
    xticks = np.linspace(0, 180, 4)

    if args.figsize is None:
        args.figsize = (8, 6)

    plt.figure(figsize=args.figsize)

    # Make sure each data set has a label.
    while len(args.label) < len(args.data):
        path = args.data[len(args.label)]
        args.label.append(label_from_filename(path))

    global_normalization = None
    for idx, data in enumerate(args.data):
        df = read_csv(data)
        label = args.label[idx]

        # If any of the angles are less than 0 then we should set xlim to -pi
        # to pi
        if (df['angle'].values < 0).any():
            args.xlim[0] = -1
            xticks = np.linspace(-180, 180, 7)

        # On the first data set, we need to get a global normalization value.
        if global_normalization is None:
            if args.normalize and not args.normalize_individually:
                global_normalization = _get_normalization(df, args)
            else:
                global_normalization = 1

        if args.subplots:
            rows, cols = args.subplots
            plt.subplot(rows, cols, idx + 1)
            plt.title(label)

        plot(args, df, global_normalization, label)

    # Figure out the x limits
    args.xlim *= math.pi
    if args.unit == 'degrees':
        args.xlim *= 180 / math.pi

    # Plot any Gaussian models requested.
    x = np.linspace(args.xlim[0], args.xlim[1], 360)
    for param in args.add_gaussian:
        plot_gaussian(x, param)

    # Create a legend if more than one data set was plotted.
    if args.add_gaussian or len(args.data) > 1 and not args.subplots:
        plt.legend()

    if args.subplots or args.tight:
        plt.tight_layout()

    # Force plotting space to cut off at y=0. Error bars will throw this
    # off if left as automatically chosen plot range.
    if not args.histogram and args.ylim:
        plt.ylim(args.ylim)
    elif not args.histogram:
        ylim = plt.ylim()
        plt.ylim((0, ylim[1]))

    plt.xlim(args.xlim)
    plt.xscale(args.xscale)
    plt.yscale(args.yscale)
    plt.xticks(xticks)

    savefig(sys.stdout)


def plot_gaussian(x, param):
    width, peak = param
    label = '$\sigma$=%.2f' % (width,)
    plt.plot(x, gaussian(param, x), label=label)


def plot(args, result, normalization, label):
    averages = result['average_weight']
    angle = result['angle']
    total_count = result['count']

    # Do per-dataset normalization.
    if args.normalize and args.normalize_individually:
        normalization *= _get_normalization(result, args)

    # Calculate variance for error bars
    if not args.no_yerr:
        yerr = result['weight_sem']
    else:
        yerr = None

    # Change to degrees just before plotting.
    if args.unit == 'degrees':
        angle = 180. * angle / math.pi
    else:
        assert args.unit == 'rad'

    # Plot of the relative intensity
    if args.histogram:
        width = angle[1] - angle[0]
        plt.bar(angle - 0.5 * width, total_count, width=width)
        plt.xlabel('Angle (%s)' % args.unit)
        plt.ylabel('Frequency')
    else:
        legend = False
        linestyle = ' ' if args.no_lines else '-'
        averages *= normalization

        if yerr is not None:
            yerr *= normalization
        plt.errorbar(angle, averages, linestyle=linestyle, marker='.',
                     yerr=yerr, label=label)
        plt.xlabel('Angle (%s)' % args.unit)
        plt.ylabel('Relative intensity')

        # For fit functions
        x = np.linspace(angle.min(), angle.max(), 200)

        if args.lorentzian_fit:
            legend = True
            param, _, fit = fit_lorentzian(angle, averages)
            peak, width = param
            label = 'A=%.2f, $\sigma$=%.2f Lorentzian fit' % (peak, width)
            plt.plot(x, fit(x), label=label)

        if args.gaussian_fit:
            legend = True
            _, _, fit = fit_gaussian(angle, averages)
            plt.plot(x, fit(x), label='Gaussian fit')

        if args.constrained_gaussian_fit:
            legend = True
            param, _, fit = fit_gaussian(angle, averages,
                                         force_peak=averages.max())
            width = param,
            label = '$\sigma = %.2f$ Gaussian fit' % width
            plt.plot(x, fit(x), label=label)
            sys.stderr.write('Fit parameters: %r\n' % param)

        if legend:
            plt.legend()


def _get_normalization(result, args, **kw):
    mode = args.normalize_mode
    if mode == 'sum':
        return _get_sum_normalization(result, args)
    elif mode == 'peak':
        return _get_peak_normalization(result, args)
    else:
        raise ValueError('Unhandled normalization mode: %s' % mode)


def _get_sum_normalization(result, args):
    '''
    Normalize such that the total irradiance (average_weight) is 1.

    '''
    total_weight = result['average_weight'].sum()
    return args.normalize_to / total_weight


def _get_peak_normalization(result, args):
    '''
    Normalize such that the maximum bin value is `normalize_to`.

    '''
    averages = result['average_weight']
    return np.float128(args.normalize_to / averages.max())

_bin_results = {}


def _notice(message, fp=None, newline=True, flush=False, ttyonly=True):
    if fp is None:
        fp = sys.stderr
    if ttyonly and not fp.isatty():
        return
    fp.write(message)
    if newline:
        fp.write('\n')
    if flush:
        fp.flush()


class MaffioneAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):
        namespace.normalize = True
        namespace.normalize_to = 0.025
        namespace.normalize_individually = True
        namespace.normalize_mode = 'peak'
        namespace.ylim = (0, 0.05)
        namespace.figsize = (6.6, 6)
        namespace.bins = 50


def _get_parser():
    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('data', nargs='+',
                        help='The CSV data to plot')

    # Normalization options
    group = parser.add_argument_group(title='Normalization options')
    group.add_argument('-n', '--normalize', action='store_true',
                       help='Normalize the displayed BSF')
    group.add_argument('-t', '--normalize-to', type=float, default=1.,
                       help='Value to normalize to')
    group.add_argument('-m', '--normalize-mode', default='peak',
                       choices=('sum', 'peak'),
                       help='Normalize sum of values or the peak of the BSF')
    group.add_argument('-i', '--normalize-individually', action='store_true',
                       help='Do not normalize with respect to first BSF')
    group.add_argument('--maffione', nargs=0, action=MaffioneAction,
                       help='Match normalization for Maffione et al. data')

    # Plotting space options
    group = parser.add_argument_group(title='Plotting space and layout')
    group.add_argument('-x', '--xscale', default='linear',
                       help='Change x-axis to log or linear scale')
    group.add_argument('-y', '--yscale', default='linear',
                       help='Change y-axis to log or linear scale')
    group.add_argument('--xlim', nargs=2, type=float,
                       help='Change plot limits for x-axis')
    group.add_argument('--ylim', nargs=2, type=float,
                       help='Change plot limits for y-axis')
    group.add_argument('--figsize', nargs=2, type=float,
                       help='Change figure size (inches)')
    group.add_argument('-l', '--label', default=[], action='append',
                       help='Manually override label for data set')
    group.add_argument('-u', '--unit', default='degrees',
                       help='Use degrees or radians for x-axis')
    group.add_argument('-T', '--tight', action='store_true',
                       help="Use matplotlib's tight_layout()")
    group.add_argument('-S', '--subplots', nargs=2, type=int,
                       help='Grid size for subplots '
                            '(plots each BSF in subplot)')

    # Analysis and plot marks
    group = parser.add_argument_group(title='Analysis and plot markings')
    group.add_argument('-E', '--no-yerr', default=False, action='store_true',
                       help='Do not show error bars')
    group.add_argument('-H', '--histogram', action='store_true',
                       help='Show sample count histogram')
    group.add_argument('-F', '--lorentzian-fit', action='store_true',
                       help='Fit Lorentzian by least-squares')
    group.add_argument('-G', '--gaussian-fit', action='store_true',
                       help='Fit Gaussian by least-squares')
    group.add_argument('-C', '--constrained-gaussian-fit', action='store_true',
                       help='Fit peak-constrained Gaussian by least-squares')
    group.add_argument('-L', '--no-lines', action='store_true',
                       help='Do not connect data points with lines')
    group.add_argument('--add-gaussian', nargs=2, type=float, default=[],
                       action='append',
                       help='Plot arbitrary Gaussian defined by (width, peak)')

    return parser


if __name__ == '__main__':
    main()

#!/usr/bin/env python
'''
Plot intensity vs. angle.

'''

from plotlib import read_csv

import numpy as np
import pandas as pd

import argparse
import math
import sys

CHUNKSIZE = 10000

DESCRIPTION = '''\
Reduce individual samples into bins to calculate a beam spread function.

Angle is determined by a computing the dot product of a "camera" view vector
with the `n1' column of the data. The sign of the angle is computed as ``sgn(n1
* camera.right)`` for the camera's right vector.
'''


def main():
    parser = _get_parser()
    args = parser.parse_args()

    if args.bin_range is None:
        args.bin_range = (0 if not args.signed_angles else -1,
                          1)

    result = _get_bin_result(args.data, args)
    df = pd.DataFrame(result)
    df.to_csv(sys.stdout, index=False)


def _notice(message, fp=None, newline=True, flush=False, ttyonly=True):
    if fp is None:
        fp = sys.stderr
    if ttyonly and not fp.isatty():
        return
    fp.write(message)
    if newline:
        fp.write('\n')
    if flush:
        fp.flush()


def _get_bin_result(path, args, label=None):
    # Construct the globals over each data set.
    bin_edges = np.linspace(args.bin_range[0] * math.pi,
                            args.bin_range[1] * math.pi, args.bins + 1)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    total_count = np.zeros(args.bins, dtype=np.int)
    total_weight = np.zeros(args.bins, dtype=np.float)

    # Running standard deviation
    weight_statistic = BinnedStatistic(bin_edges)
    time_statistic = BinnedStatistic(bin_edges)
    step_statistic = BinnedStatistic(bin_edges)
    ok_statistic = BinnedStatistic(bin_edges)
    aborted_statistic = BinnedStatistic(bin_edges)
    ok_step_statistic = BinnedStatistic(bin_edges)
    aborted_step_statistic = BinnedStatistic(bin_edges)
    ok_time_statistic = BinnedStatistic(bin_edges)
    aborted_time_statistic = BinnedStatistic(bin_edges)

    _notice('Binning {}'.format(path), newline=False)

    for df in read_csv(path, chunksize=CHUNKSIZE, error_bad_lines=False):
        n1 = df[['n1_x', 'n1_y', 'n1_z']].values
        try:
            view = df[['view_x', 'view_y', 'view_z']].values
            right = df[['right_x', 'right_y', 'right_z']].values
        except KeyError:
            view = np.array(args.view)
            right = np.array(args.right)

        # Compute theta values
        theta = np.arccos((-n1 * view).sum(axis=1))
        if args.signed_angles:
            dot = np.sum(n1 * right, axis=1)
            theta = np.sign(dot) * theta

        # Update the running statistics
        weight_statistic.add_observations(theta, df['sum'])
        time_statistic.add_observations(theta, df['time'])
        step_statistic.add_observations(theta, df['step'])

        # Old data sets may not have 'status' field so this is for backwards
        # compatibility. Previously, only 'ok' samples were reported which is
        # the reason for the assumption that all samples are 'ok'.
        try:
            df['status']
        except KeyError:
            df['status'] = np.repeat('ok', len(df))

        ok = (df['status'] == 'ok').values
        aborted = (df['status'] == 'aborted').values
        ok_statistic.add_observations(theta, ok)
        aborted_statistic.add_observations(theta, aborted)

        # These few statistics look at the behavior of a variable given the
        # status variable. For instance, it is nice to know what amount of our
        # time we are putting toward samples that are aborted, or what path
        # lengths tend to cause an aborted sample.
        if ok.any():
            ok_step_statistic.add_observations(theta[ok], df['step'][ok])
            ok_time_statistic.add_observations(theta[ok], df['time'][ok])
        if aborted.any():
            aborted_step_statistic.add_observations(
                theta[aborted], df['step'][aborted])
            aborted_time_statistic.add_observations(
                theta[aborted], df['time'][aborted])

        # Bin up the weights and add them to the totals.
        count, _ = np.histogram(theta, bins=bin_edges, weights=df['count'])
        weight, _ = np.histogram(theta, bins=bin_edges, weights=df['sum'])
        total_count += count
        total_weight += weight

        # Simple progress indicator
        _notice('.', newline=False, flush=True)
    _notice('\n')

    averages = total_weight / total_count

    return {
        'average_weight': averages,
        'count': total_count,
        'total_weight': total_weight,
        'angle': bin_centers,
        'weight_stdev': weight_statistic.stdev,
        'weight_variance': weight_statistic.variance,
        'weight_sem': weight_statistic.sem,
        'time_total': time_statistic.total,
        'time_mean': time_statistic.mean,
        'time_sem': time_statistic.sem,
        'time_stdev': time_statistic.stdev,
        'time_variance': time_statistic.variance,
        'step_total': step_statistic.total,  # XXX This looks extraneous
        'step_mean': step_statistic.mean,
        'step_sem': step_statistic.sem,
        'step_stdev': step_statistic.stdev,
        'step_variance': step_statistic.variance,
        'ok_total': ok_statistic.total,
        'ok_mean': ok_statistic.mean,
        'aborted_total': aborted_statistic.total,
        'aborted_mean': aborted_statistic.mean,

        # Time variable partitioned by status
        'ok_time_total': ok_time_statistic.total,
        'ok_time_mean': ok_time_statistic.mean,
        'ok_time_stdev': ok_time_statistic.stdev,
        'ok_time_sem': ok_time_statistic.sem,
        'aborted_time_total': aborted_time_statistic.total,
        'aborted_time_mean': aborted_time_statistic.mean,
        'aborted_time_stdev': aborted_time_statistic.stdev,
        'aborted_time_sem': aborted_time_statistic.sem,

        # Step variable partitioned by status
        'ok_step_total': ok_step_statistic.total,  # XXX Also extraneous
        'ok_step_mean': ok_step_statistic.mean,
        'ok_step_stdev': ok_step_statistic.stdev,
        'ok_step_sem': ok_step_statistic.sem,
        'aborted_step_total': aborted_step_statistic.total,  # XXX Again
        'aborted_step_mean': aborted_step_statistic.mean,
        'aborted_step_stdev': aborted_step_statistic.stdev,
        'aborted_step_sem': aborted_step_statistic.sem,
    }


class BinnedStatistic(object):

    def __init__(self, edges, dtype=np.float128):
        self.edges = np.asarray(edges)
        self.dtype = dtype
        size = len(self.edges) - 1
        self._knuth_m = np.zeros(size, dtype=dtype)
        self._knuth_s = np.zeros(size, dtype=dtype)
        self._knuth_n = np.zeros(size, dtype=np.int)
        self.total = np.zeros(size, dtype=dtype)

    def __len__(self):
        return len(self.edges) - 1

    @property
    def mean(self):
        '''The sample mean'''
        return np.array(self._knuth_m)

    @property
    def stdev(self):
        '''The sample standard deviation'''
        return np.sqrt(self._knuth_s / (self.count - 1))

    @property
    def sem(self):
        '''The sample standard error of the mean'''
        return self.stdev / (self.count)

    @property
    def variance(self):
        '''The sample variance'''
        return self.stdev ** 2

    @property
    def count(self):
        '''Sample counts'''
        return np.array(self._knuth_n)

    def add_observations(self, x, y):
        assert len(x) == len(y), 'Observations must have same size as x-coord'
        indices = np.digitize(x, self.edges) - 1
        bin_queue = np.array([y[indices == i] for i in xrange(len(self))])
        for i in xrange(len(self)):
            for value in bin_queue[i]:
                prev_mean = self._knuth_m[i]
                self._knuth_n[i] += 1
                dx = value - self._knuth_m[i]
                self._knuth_m[i] += dx / self._knuth_n[i]
                dx = value - self._knuth_m[i]
                self._knuth_s[i] += dx * (value - prev_mean)
                self.total[i] += value


def _get_parser():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('data')
    parser.add_argument('-b', '--bins', default=50, type=int,
                        help='Number of bins (default: 50)')
    parser.add_argument('-v', '--view', nargs=3, type=float,
                        default=(-1, 0, 0), help='Camera view vector')
    parser.add_argument('-r', '--right', nargs=3, type=float,
                        default=(0, 0, -1), help='Camera right vector')
    parser.add_argument('-s', '--signed-angles', action='store_true',
                        help='Assign a sign to every angle')
    parser.add_argument('-B', '--bin-range', nargs=2, type=float,
                        help='Bin range in multiples of pi.')
    return parser


if __name__ == '__main__':
    main()

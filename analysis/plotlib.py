'''
Library for plotting scripts.
'''
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from scipy import optimize


def savefig(filenm, *args, **kw):
    '''Save or display current plot'''
    kw.setdefault('format', 'pdf')
    if isinstance(filenm, file) and filenm.isatty():
        plt.show()
    else:
        plt.savefig(filenm, *args, **kw)


def read_csv(file_or_buf, **kw):
    '''
    Wrapper around pandas.read_csv() to automatically detect compression type
    by filename extension.

    '''
    if isinstance(file_or_buf, basestring):
        _, ext = os.path.splitext(file_or_buf)
        if ext == '.gz':
            kw.setdefault('compression', 'gzip')
        elif ext == '.bz':
            kw.setdefault('compression', 'bz2')
    return pd.read_csv(file_or_buf, **kw)


def label_from_filename(filename):
    '''
    Generate a reasonable label name from a filename.

    '''
    label = os.path.basename(filename)
    for ext in ('.csv.gz', '.csv'):
        if label.endswith(ext):
            label = label[:-len(ext)]
    return label


def gaussian(param, x):
    '''
    A standard gaussian form.

    Parameters:
        param : 2-tuple of (width, peak)
        x : function argument
    '''
    width, peak = param
    return peak * np.exp(-x ** 2 / (2 * width ** 2))


def constrained_gaussian(param, x, peak):
    '''
    Peak-constrained gaussian.

    Parameters:
        param : 1-tuple of (width,)
        x : function argument
        peak : the peak of the Gaussian
    '''
    sigma, = param
    return gaussian([sigma, peak], x)


def lorentzian(param, x):
    '''
    Lorentzian function.

    Parameters:
        param : 2-tuple of (width, peak)
        x : function argument
    '''
    width, peak = param
    return peak / ((x / width) ** 2 + 1)


def residuals(model, observations):
    '''
    Returns function which computes model residuals.

    Parameters:
        model : desired model function
        observations : observations at x
    '''
    def func(param, x, *args):
        return model(param, x, *args) - observations
    return func


def fit_model(model, x, observations, param0, args=()):
    '''
    Fit a model function using least squares.

    Parameters:
        model : A model function having signature (param, x, *args)
        x : observation x coordinates
        observations : observation list
        param0 : an initial guess for parameters
        args : arguments to pass on to the model function
    '''

    errfunc = residuals(model, observations)
    original_args = args
    args = (x,) + original_args
    param, covx = optimize.leastsq(errfunc, x0=param0, args=args)

    def fit(xx):
        return model(param, xx, *original_args)
    return param, covx, fit


def fit_lorentzian(x, observations):
    '''
    Fit a Lorentzian function to observations.
    '''
    return fit_model(lorentzian, x, observations, param0=[1, 1])


def fit_gaussian(x, observations, force_peak=None):
    '''
    Fit a Gaussian function to observations.
    '''
    if force_peak is None:
        model = gaussian
        param0 = [1, 1]
        args = ()
    else:
        model = constrained_gaussian
        param0 = [1]
        args = (force_peak,)
    return fit_model(model, x, observations, param0, args=args)
